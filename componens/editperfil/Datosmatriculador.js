import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import Modal from 'react-native-modal';
import {connect} from 'react-redux';
import {setUser, setisIntro, setUrlserver} from '../../store/actions';
import Constants from '../../Utils/Constants';
import {RadioButton} from 'react-native-paper';
class Datosmatriculador extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Matriculador: {},
      Patrocinador: {},
      matri: {},
      patri: {},
      editable: null,
      autopatri: null,
      automatri: null,
      response: '',
      responsep: null,
      btndisable: true,
      btndisablep: true,
    };
  }

  componentDidMount() {
    this.setState({
      Matriculador:
        this.props.userm.user.Matriculador == null
          ? ''
          : this.props.userm.user.Matriculador,
      Patrocinador:
        this.props.userm.user.Patrocinador == null
          ? ''
          : this.props.userm.user.Patrocinador,
    });
    console.log('abriendo');
    this.onautoMatri();
  }
  componentDidUpdate() {}
  setEditdataauto(values) {
    if (this.automatri !== this.props.user.user.id) {
      fetch(Constants.URL + '/users/' + this.props.user.user.id, {
        method: 'PATCH',
        headers: {
          Accept: 'application/json, text/plain, */*',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          Matriculador: this.state.automatri,

          Patrocinador: this.state.autopatri,
        }),
      })
        .then((res) => res.json())
        .then((res) => {
          console.log(res);

          if (res.id) {
            var users = this.props.user;
            users.user = res;
            this.props.setUser(users);
            // this.props.cancelar();
            this.setMerber(this.state.automatri);
          } else {
            console.log('error al guardar');
          }
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      this.setState({response: 'No puedes usar tu codigo para este campo'});
    }
  }
  onautoMatri(ev) {
    //  console.log(ev.target.value);

    fetch(Constants.URL + '/users?$limit=2&$sort[id]=-1', {
      method: 'GET',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
        Authorization: this.props.user.accessToken,
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (
          this.props.user.user.id !== res.data[1].id &&
          this.props.user.user.id !== res.data[1].Matriculador
        ) {
          this.setState({
            automatri: res.data[1].id,
            autopatri: res.data[1].id,
          });
        } else if (
          this.props.user.user.id !== res.data[0].id &&
          this.props.user.user.id !== res.data[0].Matriculador
        ) {
          this.setState({
            automatri: res.data[0].id,
            autopatri: res.data[0].id,
          });
        } else if (
          this.props.user.user.id !== res.data[2].id &&
          this.props.user.user.id !== res.data[2].Matriculador
        ) {
          this.setState({
            automatri: res.data[2].id,
            autopatri: res.data[2].id,
          });
        } else if (
          this.props.user.user.id !== res.data[3].id &&
          this.props.user.user.id !== res.data[3].Matriculador
        ) {
          this.setState({
            automatri: res.data[3].id,
            autopatri: res.data[3].id,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  setMerber(user) {
    fetch(Constants.URL + '/member-log', {
      method: 'POST',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id: this.props.user.user.id,
        name: this.props.user.user.name,
        parent_id: user,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);

        if (res.id) {
          this.props.cancelar();
        } else {
          console.log('error al guardar');
          if (res.errors[0].type == 'unique violation') {
            this.updateMermber(user);
          }
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  updateMermber(user) {
    fetch(Constants.URL + '/member-log/' + this.props.user.user.id, {
      method: 'PATCH',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name: this.props.user.user.name,
        parent_id: user,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);

        if (res.id) {
          this.props.cancelar();
        } else {
          console.log('error al guardar');
          if (res.errors[0].type == 'unique violation') {
          }
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  changeDatos() {
    console.log(Constants.URL);
    fetch(Constants.URL + '/users/' + this.props.user.user.id, {
      method: 'PATCH',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        Matriculador: this.state.matri.id,
        Patrocinador: this.state.patri.id,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);

        if (res.id) {
          var users = this.props.user;
          users.user = res;
          this.props.setUser(users);
          console.warn(res.id);
          console.log(this.props.cancelar);
          //this.props.cancelar();
          this.setMerber(this.state.matri.id);
        } else {
          console.warn('error al guardar');
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  validate3(user) {
    fetch(Constants.URL + '/member-log?parent_id=' + user.id, {
      method: 'GET',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
        Authorization: this.props.user.accessToken,
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (
          user.id !== this.props.user.user.id &&
          user.Matriculador !== this.props.user.user.id
        ) {
          if (res.data.length >= 3) {
            console.log('mayor');
          } else {
            console.log(res);
            this.setState({matri: user});
            this.setState({btndisable: false});
          }
        } else {
          console.log('no puede ser tu codigo');
          this.setState({
            response: 'No puede ser tu codigo o un codigo bajo su nivel',
            matri: {id: 0},
          });
          this.setState({btndisable: true});
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
  changeMa(text) {
    if (this.props.user.user.id + '' !== text) {
      console.log(text);
      this.setState({response: ''});
      this.setState({Matriculador: text});
      fetch(Constants.URL + '/users/' + text, {
        method: 'GET',
        headers: {
          Accept: 'application/json, text/plain, */*',
          'Content-Type': 'application/json',
          Authorization: this.props.user.accessToken,
        },
      })
        .then((res) => res.json())
        .then((res) => {
          console.log(res);
          this.validate3(res);
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      this.setState({response: 'No puedes usar tu codigo para este campo'});
    }
  }
  validatePatrocinador(user) {
    console.log('Validando');
    console.log(user);
    if (
      user.Patrocinador === this.props.user.user.id &&
      user.Matriculador === this.props.user.user.id
    ) {
      this.setState({
        responsep: 'No puede ser un usuario bajo tu nivel',
        patri: {id: 0},
        btndisablep: true,
      });
    } else {
      if (user.id) {
        this.setState({
          patri: user,

          btndisablep: false,
        });
      } else {
        this.setState({
          responsep: 'No es un Codigo valido',
          patri: {id: 0},
          btndisablep: true,
        });
      }
    }
  }
  changePa(text) {
    if (this.props.user.user.id + '' !== text) {
      this.setState({responsep: ''});
      this.setState({Patrocinador: text});
      fetch(Constants.URL + '/users/' + text, {
        method: 'GET',
        headers: {
          Accept: 'application/json, text/plain, */*',
          'Content-Type': 'application/json',
          Authorization: this.props.user.accessToken,
        },
      })
        .then((res) => res.json())
        .then((res) => {
          console.log(res);
          this.validatePatrocinador(res);
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      this.setState({responsep: 'No puedes usar tu codigo para este campo'});
    }
  }

  Cancelar() {
    this.setState({matri: {}, patri: {}});
    this.props.cancelar();
  }

  componentWillUnmount() {
    console.log('desmonstando');
  }
  setChecked(text) {
    this.setState({value: text});
    if (text == 'no') {
      this.setState({editable: false});
    } else {
      this.setState({editable: true});
    }
  }
  render() {
    return (
      <View>
        <Modal isVisible={this.props.isvisible}>
          <View style={styles.modal}>
            <Text style={styles.text}>Datos del Matriculador</Text>
            <Text style={{color: 'white'}}>Tienes un Matriculador</Text>
            <RadioButton.Group
              labelStyle={{color: 'white'}}
              style={styles.radiop}
              onValueChange={(value) => this.setChecked(value)}
              value={this.state.value}>
              <RadioButton.Item
                labelStyle={{color: 'white'}}
                style={styles.radio}
                color="green"
                label="SI"
                value="si"
              />
              <RadioButton.Item
                labelStyle={{color: 'white'}}
                style={styles.radio}
                color="red"
                label="NO"
                value="no"
              />
            </RadioButton.Group>

            {this.state.editable ? (
              <>
                <Text style={styles.label}>Matriculdor Codigo</Text>
                <Text>{this.state.matri.name}</Text>
                <Text>{this.state.response}</Text>
                <TextInput
                  style={styles.textinput}
                  onChangeText={(text) => this.changeMa(text)}
                  value={this.state.Matriculador + ''}
                  placeholder="Codigo de Matriculador"
                />

                <Text style={styles.label}>Patrocinador Codigo</Text>
                <Text>{this.state.patri.name}</Text>
                <Text>{this.state.responsep}</Text>
                <TextInput
                  style={styles.textinput}
                  onChangeText={(text) => this.changePa(text)}
                  value={this.state.Patrocinador + ''}
                  placeholder="Codigo de patrocinador"
                />
                {this.state.btndisable == false &&
                this.state.btndisablep == false ? (
                  <TouchableOpacity
                    style={styles.botoniniciar}
                    onPress={() => this.changeDatos()}>
                    <Text style={styles.textboton}>Guardar Cambios</Text>
                  </TouchableOpacity>
                ) : null}
              </>
            ) : this.state.editable == false ? (
              <>
                <Text style={{color: 'white', fontSize: 18, marginBottom: 10}}>
                  Has elegido que no tienes Matriculador
                </Text>

                <TouchableOpacity
                  style={styles.botoniniciar}
                  onPress={() => this.setEditdataauto()}>
                  <Text style={styles.textboton}>Guardar Cambios </Text>
                </TouchableOpacity>
              </>
            ) : null}

            <TouchableOpacity
              style={styles.botoncancelar}
              onPress={() => this.Cancelar()}>
              <Text style={styles.textbotonfb}>Cancelar</Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

    padding: 0,
    backgroundColor: '#D62227',
  },
  textinput: {
    height: 40,
    borderColor: 'yellow',
    borderWidth: 1,
    borderRadius: 40,
    marginBottom: 20,
    paddingLeft: 40,
    backgroundColor: 'white',
  },
  text: {
    color: 'white',
    padding: 10,
    fontSize: 18,
    textAlign: 'center',
    marginBottom: 20,
  },
  text2: {
    color: 'white',

    textAlign: 'center',
  },
  label: {
    color: 'white',
    paddingBottom: 5,
    margin: 0,
  },
  modal: {
    backgroundColor: 'white',
    borderRadius: 10,
    minHeight: 80,
    padding: 10,
    backgroundColor: '#D62227',
  },
  textbotonfb: {
    color: 'white',
  },
  container2: {
    marginTop: 20,
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
    alignItems: 'center',
  },
  textboton: {
    color: '#21242a',
  },
  botoniniciar: {
    marginBottom: 20,

    alignItems: 'center',
    backgroundColor: '#EED626',
    padding: 10,
    borderRadius: 40,
  },
  botoncancelar: {
    marginBottom: 20,
    color: 'white',
    alignItems: 'center',
    backgroundColor: 'grey',
    padding: 10,
    borderRadius: 40,
  },
  botonface: {
    marginBottom: 20,
    width: 300,
    alignItems: 'center',
    backgroundColor: '#365BF4',
    padding: 10,
    borderRadius: 40,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
});

const mapStateProps = (state) => {
  return {
    isIntro: state.isIntro,
    user: state.user,
    urlserver: state.urlserver,
  };
};

const mapDispatchToProps = {
  setUser,
};

export default connect(mapStateProps, mapDispatchToProps)(Datosmatriculador);
