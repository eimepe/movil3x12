import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import Modal from 'react-native-modal';
import {connect} from 'react-redux';
import {setUser, setisIntro, setUrlserver} from '../../store/actions';
import Constants from '../../Utils/Constants';
class Datoscontacto extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: props.users.email,
      mobile_no: props.users.numotro,
      numcelularl:
        props.users.numcelularllamadas == null
          ? ''
          : props.users.numcelularllamadas,
      numcelularlw:
        props.users.numcelularwhatsapp == null
          ? ''
          : props.users.numcelularwhatsapp,
      visible: props.isvisible,
    };
  }

  componentDidMount() {
    this.setState({
      email: this.props.users.email,
      mobile_no: this.props.users.numotro,
      numcelularl:
        this.props.users.numcelularllamadas == null
          ? ''
          : this.props.users.numcelularllamadas,
      numcelularlw:
        this.props.users.numcelularwhatsapp == null
          ? ''
          : this.props.users.numcelularwhatsapp,
    });
  }

  changeDatos() {
    console.log(Constants.URL);
    fetch(Constants.URL + '/users/' + this.props.user.user.id, {
      method: 'PATCH',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.email,
        numotro: this.state.mobile_no,
        numcelularllamadas: this.state.numcelularl,
        numcelularwhatsapp: this.state.numcelularlw,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);

        if (res.id) {
          var users = this.props.user;
          users.user = res;
          this.props.setUser(users);
          console.warn(res.id);
          console.log(this.props.cancelar);
          this.props.cancelar();
        } else {
          console.warn('error al guardar');
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  changeemail(text) {
    this.setState({email: text});
  }

  changemobile(text) {
    this.setState({mobile_no: text});
  }

  changeLlamadas(text) {
    this.setState({numcelularl: text});
  }

  changeWhatsapp(text) {
    this.setState({numcelularlw: text});
  }

  render() {
    return (
      <View>
        <Modal isVisible={this.props.isvisible}>
          <View style={styles.modal}>
            <Text style={styles.text}>Datos de contacto</Text>

            <Text style={styles.label}>Correo Electronico</Text>
            <TextInput
              style={styles.textinput}
              onChangeText={(text) => this.changeemail(text)}
              value={this.state.email}
              placeholder="Correo"
            />
            <Text style={styles.label}>Telefono</Text>
            <TextInput
              style={styles.textinput}
              onChangeText={(text) => this.changemobile(text)}
              value={this.state.mobile_no}
              placeholder="Telefono"
              keyboardType="numeric"
            />
            <Text style={styles.label}>Celular Llamadas</Text>
            <TextInput
              style={styles.textinput}
              onChangeText={(text) => this.changeLlamadas(text)}
              value={this.state.numcelularl + ''}
              placeholder="Celular llamadas"
              keyboardType="numeric"
            />
            <Text style={styles.label}>Celular Whatsapp</Text>
            <TextInput
              style={styles.textinput}
              onChangeText={(text) => this.changeWhatsapp(text)}
              value={this.state.numcelularlw + ''}
              placeholder="Celular Whatsapp"
              keyboardType="numeric"
            />

            <TouchableOpacity
              style={styles.botoniniciar}
              onPress={() => this.changeDatos()}>
              <Text style={styles.textboton}>Guardar Cambios</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.botoncancelar}
              onPress={this.props.cancelar}>
              <Text style={styles.textbotonfb}>Cancelar</Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

    padding: 0,
    backgroundColor: '#D62227',
  },
  textinput: {
    height: 40,
    borderColor: 'yellow',
    borderWidth: 1,
    borderRadius: 40,
    marginBottom: 20,
    paddingLeft: 40,
    backgroundColor: 'white',
  },
  text: {
    color: 'white',
    padding: 10,
    fontSize: 18,
    textAlign: 'center',
    marginBottom: 20,
  },
  label: {
    color: 'white',
    paddingBottom: 5,
    margin: 0,
  },
  modal: {
    backgroundColor: 'white',
    borderRadius: 10,
    minHeight: 80,
    padding: 10,
    backgroundColor: '#D62227',
  },
  textbotonfb: {
    color: 'white',
  },
  container2: {
    marginTop: 20,
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
    alignItems: 'center',
  },
  textboton: {
    color: '#21242a',
  },
  botoniniciar: {
    marginBottom: 20,

    alignItems: 'center',
    backgroundColor: '#EED626',
    padding: 10,
    borderRadius: 40,
  },
  botoncancelar: {
    marginBottom: 20,
    color: 'white',
    alignItems: 'center',
    backgroundColor: 'grey',
    padding: 10,
    borderRadius: 40,
  },
  botonface: {
    marginBottom: 20,
    width: 300,
    alignItems: 'center',
    backgroundColor: '#365BF4',
    padding: 10,
    borderRadius: 40,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
});

const mapStateProps = (state) => {
  return {
    isIntro: state.isIntro,
    user: state.user,
    urlserver: state.urlserver,
  };
};

const mapDispatchToProps = {
  setUser,
};

export default connect(mapStateProps, mapDispatchToProps)(Datoscontacto);
