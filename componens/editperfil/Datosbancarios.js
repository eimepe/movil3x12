import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import Modal from 'react-native-modal';
import {connect} from 'react-redux';
import {setUser, setisIntro, setUrlserver} from '../../store/actions';
import {Picker} from '@react-native-community/picker';
import Constants from '../../Utils/Constants';
class Datosbancarios extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tipo_identificacion: props.users.tipo_identificacion,
      numidentificacion: props.users.numidentificacion,
      banco: props.users.banco == undefined ? '' : props.users.banco,
      numerocuenta:
        props.users.numerocuenta == undefined ? '' : props.users.numerocuenta,
      visible: props.isvisible,
      responseerror: '',
    };
  }

  componentDidMount() {}

  changeDatos() {
    console.log(Constants.URL);
    fetch(Constants.URL + '/users/' + this.props.user.user.id, {
      method: 'PATCH',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        tipo_identificacion: this.state.tipo_identificacion,
        numidentificacion: this.state.numidentificacion,
        banco: this.state.banco,
        numerocuenta: this.state.numerocuenta,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);

        if (res.id) {
          var users = this.props.user;
          users.user = res;
          this.props.setUser(users);
          console.warn(res.id);
          console.log(this.props.cancelar);
          this.props.cancelar();
        } else {
          console.warn('error al guardar');
          this.setState({
            responseerror:
              'Fue imposible guardar sus datos. Verifiquelos y vuleva a intentar',
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  changeNumero(text) {
    this.setState({numidentificacion: text});
  }

  changeBanco(text) {
    this.setState({banco: text});
  }

  changeCuenta(text) {
    this.setState({numerocuenta: text});
  }

  render() {
    return (
      <View>
        <Modal isVisible={this.props.isvisible}>
          <View style={styles.modal}>
            <Text style={styles.text}>Datos de contacto</Text>

            <Text style={styles.label}>{this.state.responseerror}</Text>

            <Text style={styles.label}>Tipo de identificacion</Text>
            <View style={{backgroundColor: 'white', borderRadius: 30}}>
              <Picker
                mode="dialog"
                selectedValue={this.state.tipo_identificacion + ''}
                style={{height: 40, marginLeft: 30}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({tipo_identificacion: itemValue})
                }>
                <Picker.Item label="Cedula" value="1" />
                <Picker.Item label="Pasaporte" value="2" />
              </Picker>
            </View>

            <Text style={styles.label}>Numero Identificacion</Text>
            <TextInput
              style={styles.textinput}
              onChangeText={(text) => this.changeNumero(text)}
              value={this.state.numidentificacion}
              placeholder="Num Identificacion"
              keyboardType="numeric"
            />
            <Text style={styles.label}>Banco</Text>
            <TextInput
              style={styles.textinput}
              onChangeText={(text) => this.changeBanco(text)}
              value={this.state.banco + ''}
              placeholder="Banco"
            />
            <Text style={styles.label}>Numero de cuenta Bancaria</Text>
            <TextInput
              style={styles.textinput}
              onChangeText={(text) => this.changeCuenta(text)}
              value={this.state.numerocuenta + ''}
              placeholder="Numero de cuenta Bancaria"
              keyboardType="numeric"
            />

            <TouchableOpacity
              style={styles.botoniniciar}
              onPress={() => this.changeDatos()}>
              <Text style={styles.textboton}>Guardar Cambios</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.botoncancelar}
              onPress={this.props.cancelar}>
              <Text style={styles.textbotonfb}>Cancelar</Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

    padding: 0,
    backgroundColor: '#D62227',
  },
  textinput: {
    height: 40,
    borderColor: 'yellow',
    borderWidth: 1,
    borderRadius: 40,
    marginBottom: 20,
    paddingLeft: 40,
    backgroundColor: 'white',
  },
  text: {
    color: 'white',
    padding: 10,
    fontSize: 18,
    textAlign: 'center',
    marginBottom: 20,
  },
  label: {
    color: 'white',
    paddingBottom: 5,
    margin: 0,
  },
  modal: {
    backgroundColor: 'white',
    borderRadius: 10,
    minHeight: 80,
    padding: 10,
    backgroundColor: '#D62227',
  },
  textbotonfb: {
    color: 'white',
  },
  container2: {
    marginTop: 20,
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
    alignItems: 'center',
  },
  textboton: {
    color: '#21242a',
  },
  botoniniciar: {
    marginBottom: 20,

    alignItems: 'center',
    backgroundColor: '#EED626',
    padding: 10,
    borderRadius: 40,
  },
  botoncancelar: {
    marginBottom: 20,
    color: 'white',
    alignItems: 'center',
    backgroundColor: 'grey',
    padding: 10,
    borderRadius: 40,
  },
  botonface: {
    marginBottom: 20,
    width: 300,
    alignItems: 'center',
    backgroundColor: '#365BF4',
    padding: 10,
    borderRadius: 40,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
});

const mapStateProps = (state) => {
  return {
    isIntro: state.isIntro,
    user: state.user,
    urlserver: state.urlserver,
  };
};

const mapDispatchToProps = {
  setUser,
};

export default connect(mapStateProps, mapDispatchToProps)(Datosbancarios);
