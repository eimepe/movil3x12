import React from 'react';
import {
  Image,
  PixelRatio,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import {setUser, setisIntro, setUrlserver} from '../../store/actions';
import ImagePicker from 'react-native-image-picker';
import Constants from '../../Utils/Constants';

class Imageperfil extends React.Component {
  state = {
    avatarSource: null,
    videoSource: null,
    base64: null,
    btn: false,
  };

  constructor(props) {
    super(props);

    this.selectPhotoTapped = this.selectPhotoTapped.bind(this);
    this.selectVideoTapped = this.selectVideoTapped.bind(this);
  }

  selectPhotoTapped() {
    const options = {
      title: 'Elija una opcion',
      takePhotoButtonTitle: 'Tomar una foto',
      chooseFromLibraryButtonTitle: 'Seleccionar de la galeria',
      quality: 0.5,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        let source = {uri: response.uri};

        // console.log(response.data);
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source,
          base64: response.data,
          btn: true,
        });
      }
    });
  }

  selectVideoTapped() {
    const options = {
      title: 'Elija una opcion',
      takePhotoButtonTitle: 'Tomar una foto',
      chooseFromLibraryButtonTitle: 'Seleccionar de la galeria',
      mediaType: 'video',
      videoQuality: 'medium',
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled video picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        this.setState({
          videoSource: response.uri,
        });
      }
    });
  }
  Guardarimagen() {
    //console.log(this.state.base64);
    fetch(Constants.URL + '/users/' + this.props.user.user.id, {
      method: 'PATCH',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        picture_url: 'data:image/jpeg;base64,' + this.state.base64,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);

        if (res.id) {
          var users = this.props.user;
          users.user = res;
          this.props.setUser(users);
          console.warn(res.id);

          this.props.navigation.replace('Home');
        } else {
          console.warn('error al guardar');
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={{fontSize: 20, margin: 50}}>Cambiar Foto de perfil</Text>
        <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
          <View
            style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
            {this.state.avatarSource === null ? (
              <Text>Seleccionar </Text>
            ) : (
              <Image style={styles.avatar} source={this.state.avatarSource} />
            )}
          </View>
        </TouchableOpacity>

        {this.state.btn ? (
          <TouchableOpacity
            style={{backgroundColor: 'red', padding: 20, borderRadius: 10}}
            onPress={this.Guardarimagen.bind(this)}>
            <View>
              <Text style={{color: 'white'}}>Guardar</Text>
            </View>
          </TouchableOpacity>
        ) : null}

        {this.state.videoSource && (
          <Text style={{margin: 8, textAlign: 'center'}}>
            {this.state.videoSource}
          </Text>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    borderRadius: 75,
    width: 150,
    height: 150,
  },
});
const mapStateProps = (state) => {
  return {
    isIntro: state.isIntro,
    user: state.user,
    urlserver: state.urlserver,
  };
};

const mapDispatchToProps = {
  setUser,
};

export default connect(mapStateProps, mapDispatchToProps)(Imageperfil);
