import React from 'react'
import { View, Text, StyleSheet,TouchableOpacity, TextInput, SafeAreaView, ScrollView} from 'react-native'
import Modal from 'react-native-modal';
import { connect } from 'react-redux';
import {setUser, setisIntro, setUrlserver} from '../../store/actions'
import Constants from '../../Utils/Constants'
class Datosresidencia extends React.Component{

    constructor(props){
        super(props);
        this.state ={
            nacionalidad: props.users.nacionalidad,
            ciudad: props.users.ciudad,
            corregimiento: props.users.corregimiento,
            barriada: props.users.barriada,
            calle: props.users.calle,
            casa: props.users.casa,
            ph: props.users.ph,
            apartamento: props.users.apartamento,
            aclaracion: props.users.aclaracion
        }
    }


    changeDatos(){ 
        
      console.log(Constants.URL);
      fetch(Constants.URL+'/users/'+this.props.user.user.id, {
       method: 'PATCH',
       headers: {
         'Accept': 'application/json, text/plain, */*',
         'Content-Type': 'application/json',
        
       },
       body: JSON.stringify({
        "nacionalidad": this.state.nacionalidad,
            "ciudad": this.state.ciudad,
            "corregimiento": this.state.corregimiento,
            "barriada": this.state.barriada,
            "calle": this.state.calle,
            "casa": this.state.casa,
            "ph": this.state.ph,
            "apartamento": this.state.apartamento,
            "aclaracion": this.state.aclaracion
       }),
  
      }).then(res=>res.json())
       .then(res => {
         console.log(res);
         
       if(res.id){
        
        var users = this.props.user;
        users.user = res;
        this.props.setUser(users);
        console.warn(res.id);
        console.log(this.props.cancelar);
        this.props.cancelar();
         
       }else{
         console.warn("error al guardar");
         
       }
       }).catch((error) => {
         console.log(error);
         
       });
  

     }

    componentDidMount(){
        this.setState();
    }
    changeNacionalidad(text){
      this.setState({nacionalidad: text});
    }
    changeCiudad(text){
      this.setState({ciudad: text});
    }
    changeCorre(text){
      this.setState({corregimiento: text});
    }
    changeBarriada(text){
      this.setState({barriada: text});
    }
    changeCalle(text){
      this.setState({calle: text});
    }
    changeCasa(text){
      this.setState({casa: text});
    }

    changePh(text){
      this.setState({ph: text});
    }
    changeApartamento(text){
      this.setState({apartamento: text});
    }
    changeAclaracion(text){
      this.setState({aclaracion: text});
    }
    render(){
        return(
            <View>
              <Modal isVisible={this.props.isvisible}>
                    <SafeAreaView style={styles.modal}>
                      <ScrollView>

                      
                    <Text style={styles.text}>Datos de contacto</Text>

                    <Text style={styles.label}>Nacionalidad</Text>
                    <TextInput
                              style={styles.textinput}
                              onChangeText={text => this.changeNacionalidad(text)}
                              value={this.state.nacionalidad}
                            
                              placeholder="Nacionalidad"
                          />
                    <Text style={styles.label}>Ciudad</Text>
                    <TextInput
                             style={styles.textinput}
                              onChangeText={text => this.changeCiudad(text)}
                              value={this.state.ciudad}
                            
                              placeholder="Ciudad"
                          />
                    <Text style={styles.label}>Corregimiento </Text>
                    <TextInput
                              style={styles.textinput}
                              onChangeText={text => this.changeCorre(text)}
                              value={this.state.corregimiento}
                            
                              placeholder="Corregimiento"
                          />
                    <Text style={styles.label}>Barriada</Text>
                    <TextInput
                              style={styles.textinput}
                              onChangeText={text => this.changeBarriada(text)}
                              value={this.state.barriada}
                            
                              placeholder="Barriada"
                          />

<Text style={styles.label}>Calle</Text>
                    <TextInput
                              style={styles.textinput}
                              onChangeText={text => this.changeCalle(text)}
                              value={this.state.calle}
                            
                              placeholder="Calle"
                          />

<Text style={styles.label}>Casa </Text>
                    <TextInput
                              style={styles.textinput}
                              onChangeText={text => this.changeCasa(text)}
                              value={this.state.casa}
                            
                              placeholder="Casa"
                          />

<Text style={styles.label}>PH </Text>
                    <TextInput
                              style={styles.textinput}
                              onChangeText={text => this.changePh(text)}
                              value={this.state.ph}
                            
                              placeholder="PH"
                          />

<Text style={styles.label}>Apartamento</Text>
                    <TextInput
                              style={styles.textinput}
                              onChangeText={text => this.changeApartamento(text)}
                              value={this.state.apartamento}
                            
                              placeholder="Apartamento"
                          />

<Text style={styles.label}>Aclaracion</Text>
                    <TextInput
                              style={styles.textinput}
                              onChangeText={text => this.changeAclaracion(text)}
                              value={this.state.aclaracion}
                            
                              placeholder="Aclaracion"
                          />

                          

                            <TouchableOpacity
                              style={styles.botoniniciar}
                              onPress={()=>this.changeDatos()}
                          >
                              <Text style={styles.textboton}>Guardar Cambios</Text>
                          </TouchableOpacity>

                          <TouchableOpacity
                              style={styles.botoncancelar}
                              onPress={this.props.cancelar}
                          >
                              <Text style={styles.textbotonfb}>Cancelar</Text>
                          </TouchableOpacity>
                      </ScrollView>
                    </SafeAreaView>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        
        padding: 0,
        backgroundColor: '#D62227', 

    },
    textinput:{
        height: 40, 
        borderColor: 'yellow', 
        borderWidth: 1, 
        borderRadius: 40, 
        marginBottom: 20, 
        paddingLeft: 40, 
        backgroundColor: 'white' },
    text:{
        color: 'white',
        padding: 10,
        fontSize: 18,
        textAlign: "center",
        marginBottom: 20
    },
    label:{
        color: 'white',
        paddingBottom: 5,
        margin: 0
    },
    modal: {
      backgroundColor: 'white',
      borderRadius: 10,
      minHeight: 80,
      padding: 10,
      backgroundColor: '#D62227'
    },
    textbotonfb:{
        color: "white"
      },
    container2: {
        marginTop: 20,
        flex: 1,
        justifyContent: "center",
        paddingHorizontal: 10,
        alignItems: "center"
      },
    textboton:{
      color: "#21242a"
    },
    botoniniciar: {
      marginBottom: 20,

      alignItems: "center",
      backgroundColor: "#EED626",
      padding: 10,
      borderRadius: 40
    },
    botoncancelar: {
        marginBottom: 20,
        color: 'white',
        alignItems: "center",
        backgroundColor: "grey",
        padding: 10,
        borderRadius: 40
      },
    botonface: {
      marginBottom: 20,
      width: 300,
      alignItems: "center",
      backgroundColor: "#365BF4",
      padding: 10,
      borderRadius: 40
    },
    countContainer: {
      alignItems: "center",
      padding: 10
    }
  });

  const mapStateProps = (state)=>{
    return{
        isIntro: state.isIntro,
        user: state.user,
        urlserver: state.urlserver
    }
  }
  
  
  
  const mapDispatchToProps = {
    setUser
  }

export default connect(mapStateProps, mapDispatchToProps) (Datosresidencia);