import React, {Component} from 'react';
import {
  TouchableOpacity,
  StatusBar,
  ImageBackground,
  StyleSheet,
  View,
  Text,
} from 'react-native';
import {connect} from 'react-redux';
import Constants from '../Utils/Constants';
class Item extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity style={styles.item} onPress={this.props.noticiaview}>
        <ImageBackground
          source={{
            uri: Constants.URLFILES + '/images/news/' + this.props.photo,
          }}
          style={styles.image}>
          <View style={styles.textsection}>
            <Text style={styles.title}>{this.props.title}</Text>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
    backgroundColor: '#D62227',
  },
  item: {
    flex: 1,
    backgroundColor: '#f9c2ff',
    minHeight: 200,
    marginVertical: 5,
    marginHorizontal: 5,
    borderRadius: 5,
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  image: {
    flex: 1,
    padding: 0,
    resizeMode: 'contain',
    justifyContent: 'flex-end',
    borderRadius: 5,
    overflow: 'hidden',
  },
  textsection: {
    backgroundColor: 'rgba(0, 0, 0, 0.65)',
    borderBottomStartRadius: 5,
    borderBottomEndRadius: 5,
  },
  title: {
    fontSize: 10,
    padding: 10,
    color: 'white',
  },
});

const mapStateProps = (state) => {
  return {
    isIntro: state.isIntro,
    urlserver: state.urlserver,
    user: state.user,
    noticias: state.noticias,
  };
};

export default connect(mapStateProps, null)(Item);
