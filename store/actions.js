export const setUser = (user)=>{
    return {
        type: 'SET_USER',
        user: user
    }
}

export const setNoticias = (noticias)=>{
    return {
        type: 'SET_NOTICIAS',
        noticias: noticias
    }
}

export const clearUser = ()=>{
    return {
        type: 'CLEAR_USER'
    }
}

export const setIsintro = (isIntro)=>{
    return {
        type: 'SET_INTRO',
        isIntro: isIntro
    }
}

export const setUrlserver = (urlserver)=>{
    return {
        type: 'SET_URL',
        urlserver: urlserver
    }
}
