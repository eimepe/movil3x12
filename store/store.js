import { createStore, combineReducers} from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';





function user(state={},action){
    switch (action.type) {
        case 'SET_USER':
            return action.user;
        case 'CLEAR_USER':
            return {};
        default:
            return state;
    }
    
}

function noticias(state=[],action){
    switch (action.type) {
        case 'SET_NOTICIAS':
            return action.noticias;
        case 'CLEAR_NOTICIAS':
            return [];
        default:
            return state;
    }
    
}

function isIntro(state=false,action){
    switch (action.type) {
        case 'SET_INTRO':
            return action.isIntro;
        case 'CLEAR_INTRO':
            return false;
        default:
            return state;
    }
    
}

function urlserver(state='http://3.19.39.85:3030',action){
    switch (action.type) {
        case 'SET_URL':
            return action.urlserver;
        case 'CLEAR_URL':
            return '';
        default:
            return state;
    }
    
}




let rootReducer = combineReducers({
    user: user,
    isIntro:isIntro,
    urlserver:urlserver,
    noticias
    
});





const rootPersistConfig = { 
    key: 'root', 
    storage: AsyncStorage, 
  }; 

const pReducer = persistReducer(rootPersistConfig, rootReducer);

export const store = createStore(pReducer);
export const persistor = persistStore(store);