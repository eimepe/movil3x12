import React, {Component} from 'react';
import {
  View,
  Image,
  ImageBackground,
  FlatList,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {connect} from 'react-redux';
import {setUser, setisIntro, setUrlserver, setNoticias} from '../store/actions';
import Item from '../componens/Item';
import Constants from '../Utils/Constants';
class Noticias extends Component {
  constructor() {
    super();
  }

  componentDidMount() {
    this.getNoticias();
  }

  getNoticias() {
    fetch(Constants.URL + '/news?$limit=1000', {
      method: 'GET',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('Procesar');

        if (res.total > 0) {
          this.props.setNoticias(res.data);
          this.setState({faqs: res.data});
          console.log(res.data);
        } else {
          console.warn('error de autenticacion');
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  noticiaviewsend(item, ev) {
    this.props.navigation.navigate('Noticiaview', {
      itemId: item,
    });
  }

  render() {
    var renderItem = ({item}) => (
      <Item
        title={item.title}
        id={item.id}
        photo={item.photo}
        noticiaview={this.noticiaviewsend.bind(this, item)}
      />
    );
    if (!this.props.user.user) {
      return <View></View>;
    } else {
      return (
        <SafeAreaView style={styles.container}>
          <FlatList
            data={this.props.noticias}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
            numColumns={2}
          />
        </SafeAreaView>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#D62227',
  },
  item: {
    flex: 1,
    backgroundColor: '#f9c2ff',
    minHeight: 200,
    marginVertical: 5,
    marginHorizontal: 5,
    borderRadius: 5,
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  image: {
    flex: 1,
    padding: 0,
    resizeMode: 'contain',
    justifyContent: 'flex-end',
    borderRadius: 5,
    overflow: 'hidden',
  },
  textsection: {
    backgroundColor: 'rgba(0, 0, 0, 0.65)',
    borderBottomStartRadius: 5,
    borderBottomEndRadius: 5,
  },
  title: {
    fontSize: 10,
    padding: 10,
    color: 'white',
  },
});

const mapStateProps = (state) => {
  return {
    isIntro: state.isIntro,
    urlserver: state.urlserver,
    user: state.user,
    noticias: state.noticias,
  };
};

const mapDispatchToProps = {
  setisIntro,
  setUser,
  setUrlserver,
  setNoticias,
};
export default connect(mapStateProps, mapDispatchToProps)(Noticias);
