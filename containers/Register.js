import React from 'react';

import {connect} from 'react-redux';
import {
  View,
  Linking,
  Text,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import {Appbar, Button} from 'react-native-paper';
import Modal from 'react-native-modal';
import {
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager,
} from 'react-native-fbsdk';
import Constants from '../Utils/Constants';

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isvisible: false,
      visibleok: false,
      contentmodalok: '',
      contentmodal: '',
      email: '',
      userInfo: {},
      rescelular: null,
      resname: null,
      resemail: null,
      respassword: null,
      password: null,
      loginf: false,
    };
  }
  componentDidMount() {
    // console.log(this.props);
  }

  _goBack = () => {
    this.props.navigation.goBack();
  };

  _handleSearch = () => console.log('Searching');

  _handleMore = () => console.log('Shown more');

  //Registro Desde facebook
  logoutWithFacebook = () => {
    LoginManager.logOut();
    this.setState({userInfo: {}});
  };

  getInfoFromToken = (token) => {
    const PROFILE_REQUEST_PARAMS = {
      fields: {
        string: 'id, name, email, first_name, last_name, gender',
      },
    };
    const profileRequest = new GraphRequest(
      '/me',
      {token, parameters: PROFILE_REQUEST_PARAMS},
      (error, user) => {
        if (error) {
          console.log('login info has error: ' + error);
        } else {
          this.setState({userInfo: user, email: user.email, loginf: true});
          console.log('result:', user);
          this.register2(user);
        }
      },
    );
    new GraphRequestManager().addRequest(profileRequest).start();
  };

  register2(user) {
    // console.log(this.props.navigation.navigate("Register2", {userdata: user}));
  }
  loginWithFacebook = () => {
    // Attempt a login using the Facebook login dialog asking for default permissions.
    LoginManager.logInWithPermissions(['public_profile']).then(
      (login) => {
        if (login.isCancelled) {
          console.log('Login cancelled');
        } else {
          AccessToken.getCurrentAccessToken().then((data) => {
            const accessToken = data.accessToken.toString();
            this.getInfoFromToken(accessToken);
          });
        }
      },
      (error) => {
        console.log('Login fail with error: ' + error);
      },
    );
  };

  //fin Registro esde facebook
  changename(text) {
    var user = this.state.userInfo;
    if (text != '') {
      user.name = text;
    } else {
      user.name = ' ';
    }

    this.setState({userInfo: user});
  }

  changecelular(text) {
    var user = this.state.userInfo;
    if (text != '') {
      user.celular = text;
    } else {
      user.celular = ' ';
    }

    this.setState({userInfo: user});
  }

  changeemail(text) {
    this.setState({email: text});
  }

  changepassword(text) {
    this.setState({password: text});
  }

  Next() {
    if (!this.state.userInfo.name) {
      this.setState({resname: true});
    } else if (!this.state.userInfo.celular) {
      this.setState({rescelular: true});
    } else if (!this.state.email) {
      this.setState({resemail: true});
    } else if (!this.state.password) {
      this.setState({respassword: true});
    } else {
      var userf = this.state.userInfo;

      if (this.state.email) {
        userf.email = this.state.email;
      }

      if (this.state.password) {
        userf.password = this.state.password;
      } else {
        this.setState({respassword: true});
      }

      fetch(Constants.URL + '/users', {
        method: 'POST',
        headers: {
          Accept: 'application/json, text/plain, */*',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          role: 'Customer',
          username: this.state.userInfo.email,
          email: this.state.userInfo.email,
          password: this.state.userInfo.password,
          name: this.state.userInfo.name,
          facebookId: this.state.userInfo.id ? this.state.userInfo.id : null,
        }),
      })
        .then((res) => res.json())
        .then((res) => {
          console.log(res);
          if (res.code == 400) {
            if (res.errors[0].type == 'unique violation') {
              if (res.errors[0].path == 'face') {
                this.setState({
                  isvisible: true,
                  contentmodal:
                    'Este Facebook ya existe en nuestro sistema intenta iniciar sesion.',
                });
              } else if (res.errors[0].path == 'email') {
                this.setState({
                  isvisible: true,
                  contentmodal:
                    'Este correo ya existe en nuestro sistema intenta iniciar sesion.',
                });
              }
            }
          } else {
            this.setState({
              visibleok: true,
              contentmodalok:
                'Te has registrado correctamente ya puedes iniciar sesion .',
            });
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }

  render() {
    const isLogin = this.state.loginf;
    const buttonText = isLogin
      ? 'Cancelar Registro con  facebook'
      : 'Registrarme con Facebook';
    const onPressButton = isLogin
      ? this.logoutWithFacebook
      : this.loginWithFacebook;

    return (
      <View style={styles.container}>
        <Appbar.Header>
          <Appbar.BackAction onPress={this._goBack.bind(this)} />
          <Appbar.Content title="Registrarse" />
        </Appbar.Header>
        <SafeAreaView style={styles.container}>
          <ScrollView>
            <View style={styles.container2}>
              {this.state.resname ? (
                <Text style={{color: 'white'}}>
                  El campo Nombre es obligatorio
                </Text>
              ) : null}
              <TextInput
                style={{
                  width: 300,
                  height: 40,
                  borderColor: 'yellow',
                  borderWidth: 1,
                  borderRadius: 40,
                  marginBottom: 30,
                  paddingLeft: 40,
                  backgroundColor: 'white',
                }}
                onChangeText={(text) => this.changename(text)}
                value={this.state.userInfo.name}
                placeholder="Nombre"
              />
              {this.state.rescelular ? (
                <Text style={{color: 'white'}}>
                  El campo celular es obligatorio
                </Text>
              ) : null}

              <TextInput
                style={{
                  width: 300,
                  height: 40,
                  borderColor: 'yellow',
                  borderWidth: 1,
                  borderRadius: 40,
                  marginBottom: 30,
                  paddingLeft: 40,
                  backgroundColor: 'white',
                }}
                onChangeText={(text) => this.changecelular(text)}
                value={this.state.userInfo.celular}
                placeholder="Celular *"
              />
              {this.state.resemail ? (
                <Text style={{color: 'white'}}>
                  El campo Correo es obligatorio
                </Text>
              ) : null}
              <TextInput
                style={{
                  width: 300,
                  height: 40,
                  borderColor: 'yellow',
                  borderWidth: 1,
                  borderRadius: 40,
                  marginBottom: 30,
                  paddingLeft: 40,
                  backgroundColor: 'white',
                }}
                onChangeText={(text) => this.changeemail(text)}
                value={this.state.email}
                placeholder="Correo"
              />
              {this.state.respassword ? (
                <Text style={{color: 'white'}}>
                  El campo Contraseña es obligatorio
                </Text>
              ) : null}
              <TextInput
                style={{
                  width: 300,
                  height: 40,
                  borderColor: 'yellow',
                  borderWidth: 1,
                  borderRadius: 40,
                  marginBottom: 30,
                  paddingLeft: 40,
                  backgroundColor: 'white',
                }}
                onChangeText={(text) => this.changepassword(text)}
                value={this.state.password}
                placeholder="Contraseña"
              />
              <Text style={styles.textbotonfb}>
                Al hacer clic en Registrame, aceptas los
              </Text>
              <Text
                style={{color: '#365BF4', marginBottom: 20, fontSize: 18}}
                onPress={() => this.props.navigation.navigate('Register2')}>
                Terminos y condiciones
              </Text>
              <TouchableOpacity
                style={styles.botoniniciar}
                onPress={this.Next.bind(this)}>
                <Text style={styles.textboton}>Registrame</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={onPressButton}
                style={styles.botonface}>
                <Text style={styles.textbotonfb}>{buttonText}</Text>
              </TouchableOpacity>
              {this.state.loginf && (
                <Text style={styles.textbotonfb}>
                  Hola {this.state.userInfo.name} No hemos logrado obtener
                  algunos datos de facebook, Puedes escribirlos y dar en el
                  boton siguiente para continuar con tu registro.
                </Text>
              )}
            </View>
          </ScrollView>
        </SafeAreaView>
        <Modal isVisible={this.state.isvisible}>
          <View style={styles.modal}>
            <Text>{this.state.contentmodal}</Text>

            <Button onPress={() => this.setState({isvisible: false})}>
              Aceptar
            </Button>
          </View>
        </Modal>

        <Modal isVisible={this.state.visibleok}>
          <View style={styles.modal}>
            <Text>{this.state.contentmodalok}</Text>

            <Button onPress={this._goBack.bind(this)}>
              Ir a iniciar sesion
            </Button>
          </View>
        </Modal>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,

    padding: 0,
    backgroundColor: '#D62227',
  },
  modal: {
    backgroundColor: 'white',
    borderRadius: 10,
    minHeight: 80,
    padding: 10,
  },
  textbotonfb: {
    color: 'white',
  },
  container2: {
    marginTop: 20,
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
    alignItems: 'center',
  },
  textboton: {
    color: '#21242a',
  },
  botoniniciar: {
    marginBottom: 20,
    width: 300,
    alignItems: 'center',
    backgroundColor: '#EED626',
    padding: 10,
    borderRadius: 40,
  },
  botonface: {
    marginBottom: 20,
    width: 300,
    alignItems: 'center',
    backgroundColor: '#365BF4',
    padding: 10,
    borderRadius: 40,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
});

const mapStateProps = (state) => {
  return {
    isIntro: state.isIntro,
    urlserver: state.urlserver,
    user: state.user,
  };
};

export default connect(mapStateProps, null)(Register);
