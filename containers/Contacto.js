import * as React from 'react';
//import { TextInput } from 'react-native-paper';
import {
  View,
  TextInput,
  Image,
  Text,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
} from 'react-native';
import {connect} from 'react-redux';
import {setUser, setisIntro, setUrlserver} from '../store/actions';
import FBLoginButton from './FBLoginButton';
import Constants from '../Utils/Constants';
import {Appbar, Button} from 'react-native-paper';
import Modal from 'react-native-modal';
import {ScrollView} from 'react-native-gesture-handler';
class Contacto extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      asunto: '',
      mensaje: '',
      phone: this.props.user.user.numcelularllamadas,
      isvisible: false,
      response: null,
    };
  }

  componentDidMount() {
    //this.props.setUrlserver("http://192.168.0.7:3030");
  }

  sendContact() {
    console.log(this.props.urlserver);
    fetch(Constants.URL + '/contactenos', {
      method: 'POST',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.props.user.user.email,
        asunto: this.state.asunto,
        mensaje: this.state.mensaje,
        telefono: this.state.phone,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);

        if (res.email) {
          this.setState({
            response:
              'Mensaje enviado corretamente pronto nos cantactaremos contigo.',
            asunto: '',
            mensaje: '',
            phone: '',
            isvisible: true,
          });
        } else {
          this.setState({
            response: 'Error al enviar el mensaje',
            isvisible: true,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  onChangeText(text) {
    console.log(text);
    this.setState({asunto: text});
  }
  onChangeTextPhone(text) {
    console.log(text);
    this.setState({phone: text});
  }

  onChangeTextP(text) {
    console.log(text);
    this.setState({mensaje: text});
  }

  render() {
    if (!this.props.user.user) {
      return <View></View>;
    } else {
      return (
        <SafeAreaView
          style={{
            flex: 1,
            padding: 30,
            backgroundColor: '#D62227',
            alignItems: 'center',
          }}>
          <ScrollView>
            <Text style={{fontSize: 20, color: 'white', marginBottom: 20}}>
              Contacto
            </Text>
            <TextInput
              style={{
                width: 300,
                height: 40,
                borderColor: 'yellow',
                borderWidth: 1,
                borderRadius: 40,
                marginBottom: 30,
                paddingLeft: 40,
                backgroundColor: 'white',
              }}
              onChangeText={(text) => this.onChangeText(text)}
              value={this.state.asunto}
              placeholder="Asunto"
              multiline={true}
            />
            <TextInput
              style={{
                width: 300,
                height: 40,
                borderColor: 'yellow',
                borderWidth: 1,
                borderRadius: 40,
                marginBottom: 30,
                paddingLeft: 40,
                backgroundColor: 'white',
              }}
              onChangeText={(text) => this.onChangeTextPhone(text)}
              value={this.state.phone}
              placeholder="Telefono"
              multiline={true}
            />
            <TextInput
              style={{
                width: 300,
                height: 200,
                borderColor: 'yellow',
                borderWidth: 1,
                borderRadius: 40,
                marginBottom: 30,
                paddingLeft: 40,
                backgroundColor: 'white',
              }}
              onChangeText={(text) => this.onChangeTextP(text)}
              value={this.state.mensaje}
              placeholder="Mensaje"
              multiline={true}
            />
            <TouchableOpacity
              style={styles.botoniniciar}
              onPress={this.sendContact.bind(this)}>
              <Text style={styles.textboton}>Enviar</Text>
            </TouchableOpacity>
            <Modal isVisible={this.state.isvisible}>
              <View style={styles.modal}>
                <Text>{this.state.response}</Text>

                <Button onPress={() => this.setState({isvisible: false})}>
                  Aceptar
                </Button>
              </View>
            </Modal>
          </ScrollView>
        </SafeAreaView>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  textboton: {
    color: 'black',
  },
  botoniniciar: {
    marginBottom: 20,
    width: 300,
    alignItems: 'center',
    backgroundColor: '#EED626',
    padding: 10,
    borderRadius: 40,
  },
  modal: {
    backgroundColor: 'white',
    borderRadius: 10,
    minHeight: 80,
    padding: 10,
  },
  botonface: {
    marginBottom: 20,
    width: 300,
    alignItems: 'center',
    backgroundColor: '#365BF4',
    padding: 10,
    borderRadius: 40,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
});

const mapStateProps = (state) => {
  return {
    isIntro: state.isIntro,
    urlserver: state.urlserver,
    user: state.user,
  };
};

const mapDispatchToProps = {
  setisIntro,
  setUser,
  setUrlserver,
};

export default connect(mapStateProps, mapDispatchToProps)(Contacto);
