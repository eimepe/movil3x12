import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Vibration,
  Dimensions,
} from 'react-native';
import {WebView} from 'react-native-webview';
import {Button} from 'react-native-paper';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import {DrawerActions} from '@react-navigation/native';
import Datoscontacto from '../../componens/editperfil/Datoscontacto';
import Datosresidencia from '../../componens/editperfil/Datosresidencia';
import Datosmatriculador from '../../componens/editperfil/Datosmatriculador';
import Datosbancarios from '../../componens/editperfil/Datosbancarios';
import Constants from '../../Utils/Constants';
import {setUser} from '../../store/actions';
import ImagePicker from 'react-native-image-picker';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    alignItems: 'center',
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  logo: {
    width: 100,
    height: 100,
    borderRadius: 50,
    backgroundColor: 'white',
    borderWidth: 5,
    borderColor: 'green',
  },
  logonopago: {
    width: 100,
    height: 100,
    borderRadius: 50,
    backgroundColor: 'white',
    borderWidth: 5,
    borderColor: 'red',
  },
  logovencido: {
    width: 100,
    height: 100,
    borderRadius: 50,
    backgroundColor: 'white',
    borderWidth: 5,
    borderColor: 'orange',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-around',

    alignItems: 'center',
    padding: 20,
    backgroundColor: 'white',
    height: 200,
    width: Dimensions.get('window').width,
  },
  texthader: {
    color: 'black',
    fontSize: 18,
    fontWeight: 'bold',
  },
  content: {
    margin: 20,
  },
  title: {
    fontSize: 22,
    fontWeight: 'bold',
    color: '#666',
    marginLeft: 20,
  },
  tarjeta: {
    backgroundColor: 'white',
    padding: 20,
    marginTop: 10,
    borderRadius: 10,
  },
});
class Home extends Component {
  constructor(props) {
    super(props);
    props.navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity
          onPress={() =>
            props.navigation.dispatch(DrawerActions.toggleDrawer())
          }>
          <Text style={{marginLeft: 10}}>
            {' '}
            <Icon name="bars" size={20} color="#fff" />
          </Text>
        </TouchableOpacity>
      ),
      title: 'Mi Cuenta',
    });

    this.state = {
      isvisible: false,
      isvisibler: false,
      isvisiblem: false,
      isvisibleb: false,
      responsevacios: '',
    };

    this.cancelarContact = this.cancelarContact.bind(this);
    this.validarUser = this.validarUser.bind(this);
  }

  cancelarContact() {
    this.setState({isvisible: false});
    this.validarCamposvacios();
  }

  cancelarDResidencia() {
    this.setState({isvisibler: false});
    this.validarCamposvacios();
  }

  cancelarDMatriculador() {
    this.setState({isvisiblem: false});
    this.validarCamposvacios();
  }
  cancelarDBancarios() {
    this.setState({isvisibleb: false});
    this.validarCamposvacios();
  }
  validarCamposvacios() {
    if (
      this.props.user.user.email == null ||
      this.props.user.user.numcelularllamadas == null ||
      this.props.user.user.numcelularwhatsapp == null ||
      this.props.user.user.nacionalidad == null ||
      this.props.user.user.ciudad == null ||
      this.props.user.user.corregimiento == null ||
      this.props.user.user.barriada == null ||
      this.props.user.user.calle == null ||
      this.props.user.user.casa == null ||
      this.props.user.user.ph == null ||
      this.props.user.user.apartamento == null ||
      this.props.user.user.aclaracion == null ||
      this.props.user.user.Matriculador == null ||
      this.props.user.user.Patrocinador == null ||
      this.props.user.user.tipo_identificacion == null ||
      this.props.user.user.numidentificacion == null ||
      this.props.user.user.banco == null ||
      this.props.user.user.numerocuenta == null
    ) {
      this.setState({
        responsevacios:
          'Tu perfil esta incompleto puedes cambiar tus datos a continuacion.',
      });
    } else if (
      this.props.user.user.email == '' ||
      this.props.user.user.numcelularllamadas == '' ||
      this.props.user.user.numcelularwhatsapp == '' ||
      this.props.user.user.nacionalidad == '' ||
      this.props.user.user.ciudad == '' ||
      this.props.user.user.corregimiento == '' ||
      this.props.user.user.barriada == '' ||
      this.props.user.user.calle == '' ||
      this.props.user.user.casa == '' ||
      this.props.user.user.ph == '' ||
      this.props.user.user.apartamento == '' ||
      this.props.user.user.aclaracion == '' ||
      this.props.user.user.Matriculador == '' ||
      this.props.user.user.Patrocinador == '' ||
      this.props.user.user.tipo_identificacion == '' ||
      this.props.user.user.numidentificacion == '' ||
      this.props.user.user.banco == '' ||
      this.props.user.user.numerocuenta == ''
    ) {
      this.setState({
        responsevacios:
          'Tu perfil esta incompleto puedes cambiar tus datos a continuacion.',
      });
    } else {
      this.setState({responsevacios: ''});
    }
  }

  validarUser() {
    fetch(`${Constants.URL}/users/${this.props.user.user.id}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
        Authorization: this.props.user.accessToken,
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.code == 401) {
          this.props.setUser({});
        } else {
          if (res.id) {
            console.log('user validado');
            console.log(res.Matriculador);
            var users = this.props.user;
            users.user = res;
            this.props.setUser(users);
            if (
              this.props.user.user.Matriculador == null ||
              this.props.user.user.Matriculador == 0 ||
              this.props.user.user.Matriculador == ''
            ) {
              this.setState({isvisiblem: true});
            } else {
              console.log('Validacion correcta');
            }
          } else {
            console.log('error al guardar');
          }
        }
        // console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  componentDidMount() {
    this.validarUser();
    this.validarCamposvacios();
    //console.log('Aparece');
  }
  render() {
    if (!this.props.user.user) {
      return <View></View>;
    } else {
      return (
        <SafeAreaView style={styles.container}>
          <ScrollView>
            <View style={styles.header}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('camara')}>
                <Image
                  style={
                    this.props.user.user.status == 0 ||
                    this.props.user.user.status == 1 ||
                    this.props.user.user.status == 2
                      ? styles.logonopago
                      : this.props.user.user.status == 3
                      ? styles.logo
                      : this.props.user.user.status == 4
                      ? styles.logovencido
                      : null
                  }
                  source={{
                    uri:
                      `${Constants.URL}/images/profile/` +
                      this.props.user.user.picture_url,
                  }}
                />
              </TouchableOpacity>

              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={styles.texthader}>
                  Hola: {this.props.user.user.name}
                </Text>
                <Text style={styles.texthader}>
                  Codigo Matriculador: {this.props.user.user.id}
                </Text>

                {this.props.user.user.ifpago === 0 ? (
                  <>
                    <Text style={{color: 'red'}}>
                      Aun no has realizado tu pago inicial
                    </Text>
                    <Button
                      onPress={() =>
                        this.props.navigation.replace('pagoinicial')
                      }
                      mode="contained">
                      Realizar pago
                    </Button>
                  </>
                ) : null}
              </View>
            </View>

            <View style={styles.content}>
              <Text style={{fontSize: 18, color: 'red'}}>
                {this.state.responsevacios}
              </Text>
              <Text style={styles.title}>Datos de contacto</Text>
              <View style={styles.tarjeta}>
                <Text>Correo: {this.props.user.user.email}</Text>
                <Text>Telefono: {this.props.user.user.numotro}</Text>
                <Text>
                  Celular llamadas: {this.props.user.user.numcelularllamadas}
                </Text>
                <Text>
                  Celular Whatsapp: {this.props.user.user.numcelularwhatsapp}
                </Text>
                <Button onPress={() => this.setState({isvisible: true})}>
                  Editar
                </Button>
              </View>

              <Text style={styles.title}>Datos de Residencia</Text>
              <View style={styles.tarjeta}>
                <Text>Nacionalidad: {this.props.user.user.nacionalidad}</Text>
                <Text>Ciudad: {this.props.user.user.ciudad}</Text>
                <Text>Corregimiento: {this.props.user.user.corregimiento}</Text>
                <Text>Barriada: {this.props.user.user.barriada}</Text>

                <Text>Calle: {this.props.user.user.calle}</Text>
                <Text>Casa: {this.props.user.user.casa}</Text>
                <Text>PH: {this.props.user.user.ph}</Text>

                <Text>Apartamento: {this.props.user.user.apartamento}</Text>

                <Text>Aclaracion: {this.props.user.user.aclaracion}</Text>
                <Button onPress={() => this.setState({isvisibler: true})}>
                  Editar
                </Button>
              </View>

              <Text style={styles.title}>Mi Matriculador y Patrocinador</Text>
              <View style={styles.tarjeta}>
                <Text>
                  Matriculador :{' '}
                  {this.props.user.user.Matriculador !== null
                    ? this.props.user.user.Matriculador == 0
                      ? ''
                      : this.props.user.user.matri.name
                    : null}
                </Text>
                <Text>
                  Telefono:{' '}
                  {this.props.user.user.Matriculador !== null
                    ? this.props.user.user.Matriculador == 0
                      ? ''
                      : this.props.user.user.matri.numcelularllamadas
                    : null}
                </Text>

                <Text>
                  Patrocinador:{' '}
                  {this.props.user.user.Patrocinador !== null
                    ? this.props.user.user.patri.name
                    : null}
                </Text>
                <Text>
                  Telefono:{' '}
                  {this.props.user.user.Patrocinador !== null
                    ? this.props.user.user.patri.numcelularllamadas
                    : null}
                </Text>
                <Button onPress={() => this.setState({isvisiblem: true})}>
                  Editar
                </Button>
              </View>

              <Text style={styles.title}>
                Mis datos Bancarios e Identificacion
              </Text>
              <View style={styles.tarjeta}>
                <Text>
                  Tipo de Documento:{' '}
                  {this.props.user.user.tipo_identificacion == 1
                    ? 'Cedula'
                    : this.props.user.user.tipo_identificacion == 2
                    ? 'Pasaporte'
                    : null}
                </Text>
                <Text>Numero D: {this.props.user.user.numidentificacion}</Text>
                <Text>Banco: {this.props.user.user.banco}</Text>
                <Text>
                  Numero de cuenta: {this.props.user.user.numerocuenta}
                </Text>

                <Button onPress={() => this.setState({isvisibleb: true})}>
                  Editar
                </Button>
              </View>
            </View>
          </ScrollView>
          <Datoscontacto
            users={this.props.user.user}
            isvisible={this.state.isvisible}
            cancelar={this.cancelarContact}
          />
          <Datosresidencia
            users={this.props.user.user}
            isvisible={this.state.isvisibler}
            cancelar={this.cancelarDResidencia.bind(this)}
          />
          <Datosmatriculador
            userm={this.props.user}
            isvisible={this.state.isvisiblem}
            cancelar={this.cancelarDMatriculador.bind(this)}
          />
          <Datosbancarios
            users={this.props.user}
            isvisible={this.state.isvisibleb}
            cancelar={this.cancelarDBancarios.bind(this)}
          />
        </SafeAreaView>
      );
    }
  }
}

const mapStateProps = (state) => {
  return {
    isIntro: state.isIntro,
    user: state.user,
    urlserver: state.urlserver,
  };
};

const mapDispatchToProps = {
  setUser,
};
export default connect(mapStateProps, mapDispatchToProps)(Home);
