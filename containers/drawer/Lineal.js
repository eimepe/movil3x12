import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import TreeView from 'react-native-final-tree-view';
import Icon from 'react-native-vector-icons/FontAwesome';
import {DrawerActions} from '@react-navigation/native';
import Constants from '../../Utils/Constants';

function getIndicator(isExpanded, hasChildrenNodes) {
  if (!hasChildrenNodes) {
    return <Icon name="minus" size={15} color="#fff" />;
  } else if (isExpanded) {
    return <Icon name="arrow-circle-down" size={15} color="#fff" />;
  } else {
    return <Icon name="arrow-circle-right" size={15} color="#fff" />;
  }
}

class Lineal extends React.Component {
  constructor(props) {
    super(props);
    props.navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity
          onPress={() =>
            props.navigation.dispatch(DrawerActions.toggleDrawer())
          }>
          <Text style={{marginLeft: 10}}>
            {' '}
            <Icon name="bars" size={20} color="#fff" />
          </Text>
        </TouchableOpacity>
      ),
      title: 'Mi Equipo',
    });
    this.state = {
      family: [{id: props.user.user.id, name: props.user.user.name}],
    };
  }

  getData() {
    fetch(`${Constants.URL}/estructuras?parent_id=${this.props.user.user.id}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);

        this.setState({
          family: [
            {
              id: this.props.user.user.id,
              name: this.props.user.user.name,
              children: res.data,
            },
          ],
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  componentDidMount() {
    this.getData();
  }
  render() {
    if (!this.props.user.user) {
      return <View></View>;
    } else {
      return (
        <View style={{flex: 1, padding: 20}}>
          <TreeView
            style={{height: 40}}
            data={this.state.family} // defined above
            getCollapsedNodeHeight={() => {
              50;
            }}
            renderNode={({node, level, isExpanded, hasChildrenNodes}) => {
              return (
                <View
                  style={{
                    backgroundColor: 'grey',
                    height: 40,
                    marginTop: 10,
                    padding: 5,
                  }}>
                  <Text
                    style={{
                      marginLeft: 10 * level,
                      color: 'white',
                      fontSize: 15,
                      paddingLeft: 10,
                    }}>
                    {getIndicator(isExpanded, hasChildrenNodes)} {node.name}
                  </Text>
                </View>
              );
            }}
          />
        </View>
      );
    }
  }
}

const mapStateProps = (state) => {
  return {
    urlserver: state.urlserver,
    user: state.user,
  };
};

const mapDispatchToProps = {};
export default connect(mapStateProps, mapDispatchToProps)(Lineal);
