import React from 'react';
import WebView from 'react-native-webview';
import {
  View,
  TouchableOpacity,
  Text,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import {DrawerActions} from '@react-navigation/native';
import Constants from '../../Utils/Constants';

class Piramide extends React.Component {
  constructor(props) {
    super(props);
    props.navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity
          onPress={() =>
            props.navigation.dispatch(DrawerActions.toggleDrawer())
          }>
          <Text style={{marginLeft: 10}}>
            <Icon name="bars" size={20} color="#fff" />
          </Text>
        </TouchableOpacity>
      ),
      title: 'Mi Equipo',
    });
    this.state = {
      visible: true,
    };
  }

  hideSpinner() {
    this.setState({visible: false});
  }
  render() {
    if (!this.props.user.user) {
      return <View></View>;
    } else {
      return (
        <View style={{flex: 1}}>
          <WebView
            onLoad={() => this.hideSpinner()}
            ref={(r) => (this.webref = r)}
            source={{
              uri: `${Constants.URLWEB}/piramideapp/?iduser=${this.props.user.user.id}&status=${this.props.user.user.ifpago}&name=Codigo:${this.props.user.user.id}&title=${this.props.user.user.name}&image=${this.props.user.user.picture_url}`,
            }}
            renderLoading={this.LoadingIndicatorView}
            startInLoadingState={true}
          />

          {this.state.visible && (
            <ActivityIndicator
              style={{
                position: 'absolute',
                top: Dimensions.get('window').height / 2,
                left: Dimensions.get('window').width / 2,
              }}
              size="large"
            />
          )}
        </View>
      );
    }
  }
}

const mapStateProps = (state) => {
  return {
    urlserver: state.urlserver,
    user: state.user,
  };
};

const mapDispatchToProps = {};
export default connect(mapStateProps, mapDispatchToProps)(Piramide);
