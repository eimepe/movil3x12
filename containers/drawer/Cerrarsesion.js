import React from 'react'
import { View, Text } from 'react-native'
import { connect } from 'react-redux';
import {setUser, clearUser} from '../../store/actions'
class Cerrarsesion extends React.Component{

    componentDidMount(){
       // 
       console.log("cerrar");
        
            this.props.clearUser();
            this.props.navigation.navigate('Home');
            console.log("cerrar..");
      
        
    }

    render(){
        return(
            <View>
                <Text>
                    Cerrando sesion
                </Text>
            </View>
        )
    }
}




const mapStateProps = (state)=>{
    return{
        isIntro: state.isIntro,
        user: state.user,
        urlserver: state.urlserver
    }
  }
  
  
  
  const mapDispatchToProps = {
    setUser,
    clearUser
  }
  export default connect(mapStateProps, mapDispatchToProps)(Cerrarsesion);