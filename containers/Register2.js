import React from 'react';
import {StyleSheet, Dimensions, View} from 'react-native';
import {Appbar, Button} from 'react-native-paper';
import Pdf from 'react-native-pdf';

export default class Register2 extends React.Component {
  _goBack = () => {
    this.props.navigation.goBack();
  };
  render() {
    const source = {
      uri: 'http://3x12.net/terminosycondicines.pdf',
      cache: true,
    };

    return (
      <View style={styles.container}>
        <Appbar.Header>
          <Appbar.BackAction onPress={this._goBack.bind(this)} />
          <Appbar.Content title="Terminos y condiciones" />
        </Appbar.Header>
        <Pdf
          source={source}
          onLoadComplete={(numberOfPages, filePath) => {
            console.log(`completado: ${numberOfPages}`);
          }}
          onPageChanged={(page, numberOfPages) => {
            console.log(`pagina en este momento ${page}`);
          }}
          onError={(error) => {
            console.log(error);
          }}
          onPressLink={(uri) => {
            console.log(`link: ${uri}`);
          }}
          style={styles.pdf}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

    marginTop: 0,
  },
  pdf: {
    flex: 1,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});
