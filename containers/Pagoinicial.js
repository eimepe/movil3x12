import React from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {Appbar, Button} from 'react-native-paper';
import Modal from 'react-native-modal';
import {connect} from 'react-redux';
import {setUser, setisIntro, setUrlserver} from '../store/actions';
import Icon from 'react-native-vector-icons/FontAwesome';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ScrollView} from 'react-native-gesture-handler';
import Constants from '../Utils/Constants';
class Pagoinicial extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      type: 'NA', //VISA, MASTERCARD o NV
      visibleok: false,
      loader: true,
      responsepago: '',
      name: this.props.user.user.name,
      email: this.props.user.user.email,
      phone: '',
      direccion: '',
      numbercard: '',
      cvv: '',
      month: '',
      year: '',
      firstName: '',
      lastName: '',
      cardType: '',
      error: false,
    };
  }
  Pagar() {
    this.setState({
      visibleok: true,
      responsepago: 'Realizando pago por favor espere...',
    });
    let payload = {
      cclw:
        'D17B05A095489D1176560B4666A283454185F353F401D0201CC5C16F92535DF6B1DEBA18E79442CC0D6F75FD024207680AFBDFD6CF015478BF30CBEF9160A08D',
      amount: 15, //El monto o valor total de la transacción a realizar. NO PONER
      taxAmount: 1.0,
      email: this.state.email, //String MaxLength:100 Email del
      phone: this.state.phone, //Numeric MaxLength:16 Teléfono del Tarjeta habiente,
      address: this.state.direccion, //String MaxLength:100 Dirección del Tarjeta,
      concept: 'Pago inicial', //MaxLength:150 ;Es la descripción o el motivo de la transacción en proceso
      description: 'Pago inicial Shid', //MaxLength:150 ;Es la descripción o el motivo de la transacción en proceso
      lang: 'ES', //EN

      cardInformation: {
        cardNumber: this.state.numbercard,
        expMonth: this.state.month, //Mes de expiración de la tarjeta, siempre 2 dígitos
        expYear: this.state.year, //Numeric Ej.:02 Año de expiración de la tarjeta.
        cvv: this.state.cvv, //Código de Seguridad de la tarjeta Numeric MaxLength:3
        firstName: this.state.firstName, //String MaxLength:25 Nombre del tarjeta habiente
        lastName: this.state.lastName, //String MaxLength:25 Apellido del Tarjeta habiente
        cardType: this.state.type,
      },
    };
    fetch('https://sandbox.paguelofacil.com/rest/processTx/AUTH_CAPTURE', {
      method: 'POST',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
        authorization:
          'WT5hTaUcpa4J3h4AmrZa2EXXJs8boUVa|DIRd852djHbq2j5Fca5VDUkDbExTBCVf',
      },
      body: JSON.stringify(payload),
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.success == false) {
          this.setState({
            responsepago:
              'Ha Ocurrio un error al procesar el pago por favor comuniquese con el comercio o intente con otra tarjeta',
            loader: false,
            error: true,
          });
        }
        if (res.data.status == 0) {
          this.setState({
            responsepago:
              'Ha Ocurrio un error al procesar el pago por favor comuniquese con el comercio o intente con otra tarjeta',
            loader: false,
            error: true,
          });
          //console.log(res);
        } else if (res.data.status == 1) {
          this.getChangeuser(res.status);

          this.setState({
            responsepago: 'Su pago fue recibido con exito',
            loader: false,
            error: false,
          });
        }
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  Validate(number, ev) {
    this.setState({numbercard: number});
    if (number.match(/^4\d{3}-?\d{4}-?\d{4}-?\d{4}$/)) {
      var visa_error = 'Es Visa';
      this.setState({type: 'VISA'});
      console.log(visa_error);
    } else if (number.match(/^5[1-5]\d{2}-?\d{4}-?\d{4}-?\d{4}$/)) {
      console.log('Es marter card');
      this.setState({type: 'MASTERCARD'});
    } else {
      console.log('No valido');
      this.setState({type: 'NV'});
    }
  }

  getChangeuser(responsepago) {
    //console.log(Constants.URL);
    fetch(Constants.URL + '/users/' + this.props.user.user.id, {
      method: 'PATCH',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        ifpago: 1,
        ifpago_fecha: new Date(),
        status: 3,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        //console.log(res);
        if (res.id) {
          var users = this.props.user;
          users.user = res;
          this.props.setUser(users);

          // console.log(this.props.user);
        } else {
          console.warn('error al guardar');
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  Mes(text) {
    this.setState({month: text});
  }
  Ano(text) {
    this.setState({year: text});
  }
  CVV(text) {
    this.setState({cvv: text});
  }
  Primernombre(text) {
    this.setState({firstName: text});
  }
  Apellidos(text) {
    this.setState({lastName: text});
  }

  Nombres(text) {
    this.setState({name: text});
  }
  Email(text) {
    this.setState({email: text});
  }
  Phone(text) {
    this.setState({phone: text});
  }
  Direccion(text) {
    this.setState({direccion: text});
  }

  _goBack = () => {
    this.props.navigation.goBack();
  };
  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'red'}}>
        <Appbar.Header>
          <Appbar.BackAction onPress={this._goBack.bind(this)} />
          <Appbar.Content title="Pago inicial" />
        </Appbar.Header>
        <SafeAreaView style={{margin: 20}}>
          <ScrollView>
            <View
              style={{backgroundColor: 'white', padding: 20, borderRadius: 10}}>
              <Text style={{fontSize: 20}}>Valor a pagar USD: 15</Text>
              <Text>Por concepto: Pago inicial activacion</Text>
            </View>
            <Text
              style={{
                fontSize: 18,
                textAlign: 'center',
                color: 'white',
                margin: 10,
              }}>
              Datos del pago
            </Text>
            <TextInput
              style={styles.textinput}
              onChangeText={(text) => this.Nombres(text)}
              value={this.state.name}
              placeholder="Nombre"
            />
            <TextInput
              style={styles.textinput}
              onChangeText={(text) => this.Email(text)}
              value={this.state.email}
              textContentType="emailAddress"
              autoCompleteType="email"
              keyboardType="email-address"
              placeholder="Correo electronico"
            />
            <TextInput
              style={styles.textinput}
              onChangeText={(text) => this.Phone(text)}
              value={this.state.phone}
              placeholder="Telefono"
              keyboardType="numeric"
            />
            <TextInput
              style={styles.textinput}
              onChangeText={(text) => this.Direccion(text)}
              value={this.state.direccion}
              placeholder="Direccion"
            />

            <Text
              style={{
                fontSize: 18,
                textAlign: 'center',
                color: 'white',
                margin: 10,
              }}>
              Datos de la tarjeta
            </Text>
            {this.state.type == 'NA' ? null : this.state.type == 'VISA' ? (
              <View
                style={{
                  margin: 30,
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                }}>
                <Text style={{color: 'white'}}>Tipo de tarjeta</Text>
                <Icon name="cc-visa" color={'white'} size={80} />
              </View>
            ) : this.state.type == 'MASTERCARD' ? (
              <View
                style={{
                  margin: 30,
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                }}>
                <Text style={{color: 'white'}}>Tipo de tarjeta</Text>
                <Icon name="cc-mastercard" color={'white'} size={80} />
              </View>
            ) : this.state.type == 'NV' ? (
              <View
                style={{
                  margin: 30,
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                }}>
                <Text style={{color: 'white'}}>
                  Numero de tarjeta no valido
                </Text>
              </View>
            ) : null}

            <TextInput
              style={styles.textinput}
              onChangeText={(text) => this.Validate(text)}
              value={this.state.numbercard}
              placeholder="Numero de tarjeta "
              keyboardType="numeric"
              maxLength={19}
            />

            <TextInput
              style={styles.textinput}
              onChangeText={(text) => this.Mes(text)}
              value={this.state.month}
              placeholder="Mes de vencimieno - MM"
              keyboardType="numeric"
              maxLength={2}
            />

            <TextInput
              style={styles.textinput}
              onChangeText={(text) => this.Ano(text)}
              value={this.state.year}
              placeholder="Año de vencimieno - AA"
              keyboardType="numeric"
              maxLength={2}
            />

            <TextInput
              style={styles.textinput}
              onChangeText={(text) => this.CVV(text)}
              value={this.state.cvv}
              placeholder="CVV"
              keyboardType="numeric"
              maxLength={3}
            />

            <TextInput
              style={styles.textinput}
              onChangeText={(text) => this.Primernombre(text)}
              value={this.state.firstName}
              placeholder="Nombres"
            />

            <TextInput
              style={styles.textinput}
              onChangeText={(text) => this.Apellidos(text)}
              value={this.state.lastName}
              placeholder="Apellidos "
            />

            {this.state.name &&
            this.state.email &&
            this.state.phone &&
            this.state.direccion &&
            this.state.numbercard &&
            this.state.month &&
            this.state.year &&
            this.state.cvv &&
            this.state.firstName &&
            this.state.lastName &&
            this.state.name ? (
              <TouchableOpacity
                onPress={this.Pagar.bind(this)}
                style={styles.botoniniciar}>
                <Text style={styles.textbotonfb}>Pagar ahora</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                disabled={true}
                onPress={() => console.log('Desactivado')}
                style={styles.botondesactivado}>
                <Text style={styles.textdesactivado}>Pagar ahora</Text>
              </TouchableOpacity>
            )}

            <Modal isVisible={this.state.visibleok}>
              <View style={styles.modal}>
                {this.state.loader ? (
                  <View>
                    <ActivityIndicator size="large" color="#00ff00" />
                    <Text>{this.state.responsepago}</Text>
                  </View>
                ) : (
                  <View>
                    <Text>{this.state.responsepago}</Text>
                    {this.state.error ? (
                      <Button
                        style={{marginTop: 10}}
                        mode="contained"
                        onPress={() => {
                          //this.props.navigation.navigate('Home');
                          this.setState({visibleok: false, loader: true});
                        }}>
                        Volver a intentar
                      </Button>
                    ) : (
                      <Button
                        style={{marginTop: 10}}
                        mode="contained"
                        onPress={() => {
                          this.props.navigation.navigate('Home');
                          this.setState({visibleok: false});
                        }}>
                        Ir a mi cuenta
                      </Button>
                    )}
                  </View>
                )}
              </View>
            </Modal>
          </ScrollView>
        </SafeAreaView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,

    padding: 0,
    backgroundColor: '#D62227',
  },
  modal: {
    backgroundColor: 'white',
    borderRadius: 10,
    minHeight: 80,
    padding: 10,
  },
  textbotonfb: {
    color: 'white',
  },
  botondesactivado: {
    marginBottom: 20,

    alignItems: 'center',
    backgroundColor: 'grey',
    padding: 10,
    borderRadius: 40,

    marginBottom: 100,
  },
  textdesactivado: {
    textDecorationLine: 'line-through',
  },
  container2: {
    marginTop: 20,
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
    alignItems: 'center',
  },
  textboton: {
    color: '#21242a',
  },
  botoniniciar: {
    marginBottom: 80,

    alignItems: 'center',
    backgroundColor: 'green',
    padding: 10,
    borderRadius: 40,
  },
  botonface: {
    marginBottom: 20,
    width: 300,
    alignItems: 'center',
    backgroundColor: '#365BF4',
    padding: 10,
    borderRadius: 40,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  textinput: {
    height: 40,
    borderColor: 'yellow',
    borderWidth: 1,
    borderRadius: 40,
    marginBottom: 20,
    paddingLeft: 40,
    backgroundColor: 'white',
  },
});
const mapStateProps = (state) => {
  return {
    isIntro: state.isIntro,
    user: state.user,
    urlserver: state.urlserver,
  };
};

const mapDispatchToProps = {
  setUser,
};

export default connect(mapStateProps, mapDispatchToProps)(Pagoinicial);
