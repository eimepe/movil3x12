import * as React from 'react';
//import { TextInput } from 'react-native-paper';
import {View, TextInput, Image, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {connect} from 'react-redux'
import {setUser, setisIntro, setUrlserver} from '../store/actions'
import FBLoginButton from './FBLoginButton'
import Constants from '../Utils/Constants';
class Login extends React.Component {
 
  constructor(props){
    super(props);
    this.state = {
      pass: "",
      repetirpass: "",
      id: 0,
      iniciar: false,
      res: ""
    }
  }

 componentDidMount(){
    const { id } = this.props.route.params;
    this.setState({id: id});
 }

  changePass(){ 
      if(this.state.pass===this.state.repetirpass){
    console.log(this.props.urlserver);
    fetch(Constants.URL+'/users/'+this.state.id, {
     method: 'PATCH',
     headers: {
       'Accept': 'application/json, text/plain, */*',
       'Content-Type': 'application/json',
      
     },
     body: JSON.stringify({
      "password":this.state.pass,
     }),

    }).then(res=>res.json())
     .then(res => {
       console.log(res);
       
     if(res.id){
      
       console.warn(res);
       this.setState({iniciar: true})
       
     }else{
       console.warn("error al cambiar pass");
       
     }
     }).catch((error) => {
       console.log(error);
       
     });

    }else{
        this.setState({res: "Las contraseñas no Son iguales"})
    }
   }

   onChangeText(text){
     console.log(text);
     this.setState({pass: text})
   }

   onChangeTextP(text){
    console.log(text);
    this.setState({repetirpass: text})
  }

render(){
 
  return (
      <View style={{flex: 1,padding: 30, backgroundColor: '#D62227', alignItems: "center"}}>
        <Image source={require('../assets/logo.png')} style={{ width: 300, height: 300, }} />
        <TextInput
            style={{ width: 300, height: 40, borderColor: 'yellow', borderWidth: 1, borderRadius: 40, marginBottom: 30, paddingLeft: 40, backgroundColor: 'white' }}
            onChangeText={text => this.onChangeText(text)}
            value={this.state.pass}
            placeholder="Nueva Contraseña"
        />
        <TextInput
            style={{ width: 300, height: 40, borderColor: 'yellow', borderWidth: 1, borderRadius: 40, marginBottom: 30, paddingLeft: 40, backgroundColor: 'white' }}
            onChangeText={text => this.onChangeTextP(text)}
            value={this.state.repetirpass}
            placeholder="Repetir Contraseña"
        />

         <Text style={{color: "#ffffff", fontSize: 20}}>{this.state.res}</Text>
        <TouchableOpacity
          style={styles.botoniniciar}
          onPress={this.changePass.bind(this)}
        >
          <Text style={styles.textboton}>Enviar</Text>
        </TouchableOpacity>

        {this.state.iniciar?<>
            <Text>Contraseña Cambiada correctamente:</Text>
             <TouchableOpacity
            style={styles.botoniniciar}
            onPress={()=>this.props.navigation.navigate('Login')}
            >
            <Text style={styles.textboton}>Ir a Iniciar sesion</Text>
            </TouchableOpacity>
         </>:null
           
        }
        

     
        </View>
    
  );
}
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 10
  },
  textboton:{
    color: "white"
  },
  botoniniciar: {
    marginBottom: 20,
    width: 300,
    alignItems: "center",
    backgroundColor: "#EED626",
    padding: 10,
    borderRadius: 40
  },
  botonface: {
    marginBottom: 20,
    width: 300,
    alignItems: "center",
    backgroundColor: "#365BF4",
    padding: 10,
    borderRadius: 40
  },
  countContainer: {
    alignItems: "center",
    padding: 10
  }
});

const mapStateProps = (state)=>{
  return{
      isIntro: state.isIntro,
      urlserver: state.urlserver,
      user: state.user
  
  }
}



const mapDispatchToProps = {
  setisIntro,
  setUser,
  setUrlserver
  

}



export default connect(mapStateProps, mapDispatchToProps)(Login);
  