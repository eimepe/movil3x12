import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {setUser, setisIntro, setUrlserver} from '../store/actions';
import {
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager,
} from 'react-native-fbsdk';
import Modal from 'react-native-modal';
import {Appbar, Button} from 'react-native-paper';
import {connect} from 'react-redux';
import Constants from '../Utils/Constants';

class FBLoginButton extends Component {
  constructor() {
    super();
    this.state = {
      visibleok: false,
      contentmodalok: '',
      userInfo: {},
    };
  }

  logoutWithFacebook = () => {
    LoginManager.logOut();
    this.setState({userInfo: {}});
  };

  getInfoFromToken = (token) => {
    const PROFILE_REQUEST_PARAMS = {
      fields: {
        string: 'id,name,first_name,last_name',
      },
    };
    const profileRequest = new GraphRequest(
      '/me',
      {token, parameters: PROFILE_REQUEST_PARAMS},
      (error, user) => {
        if (error) {
          console.log('login info has error: ' + error);
        } else {
          this.setState({userInfo: user});
          console.log('result:', user.id);
          this.iniciarsesionf(user.id);
        }
      },
    );
    new GraphRequestManager().addRequest(profileRequest).start();
  };

  iniciarsesionf(faceid) {
    fetch(Constants.URL + '/loginfacebook?facebookid=' + faceid, {
      method: 'GET',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);

        if (res.accessToken) {
          this.props.setUser(res);
          console.warn(res);
          this.props.navigation.navigate('Home');
        } else {
          console.warn('error de autenticacion');
          this.setState({
            visibleok: true,
            contentmodalok:
              'No estas registrado con facebook en nuetra plataforma',
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
  loginWithFacebook = () => {
    // Attempt a login using the Facebook login dialog asking for default permissions.
    LoginManager.logInWithPermissions(['public_profile']).then(
      (login) => {
        if (login.isCancelled) {
          console.log('Login cancelled');
        } else {
          AccessToken.getCurrentAccessToken().then((data) => {
            const accessToken = data.accessToken.toString();
            this.getInfoFromToken(accessToken);
          });
        }
      },
      (error) => {
        console.log('Login fail with error: ' + error);
      },
    );
  };

  state = {userInfo: {}};

  render() {
    const isLogin = this.state.userInfo.name;
    const buttonText = isLogin
      ? 'Cerrar sesion facebook'
      : 'Iniciar con Facebook';
    const onPressButton = isLogin
      ? this.logoutWithFacebook
      : this.loginWithFacebook;
    return (
      <View style={{flex: 1, margin: 50}}>
        <TouchableOpacity onPress={onPressButton} style={styles.botonface}>
          <Text style={styles.textboton}>{buttonText}</Text>
        </TouchableOpacity>
        {this.state.userInfo.name && (
          <Text style={styles.textboton}>
            Logeado como {this.state.userInfo.name}
          </Text>
        )}

        <Modal isVisible={this.state.visibleok}>
          <View style={styles.modal}>
            <Text>{this.state.contentmodalok}</Text>

            <Button onPress={() => this.setState({visibleok: false})}>
              OK
            </Button>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  textboton: {
    color: 'white',
  },
  botoniniciar: {
    marginBottom: 20,
    width: 300,
    alignItems: 'center',
    backgroundColor: '#EED626',
    padding: 10,
    borderRadius: 40,
  },
  modal: {
    backgroundColor: 'white',
    borderRadius: 10,
    minHeight: 80,
    padding: 10,
  },
  botonface: {
    marginBottom: 20,
    width: 300,
    alignItems: 'center',
    backgroundColor: '#365BF4',
    padding: 10,
    borderRadius: 40,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
});

const mapStateProps = (state) => {
  return {
    isIntro: state.isIntro,
    urlserver: state.urlserver,
    user: state.user,
  };
};

const mapDispatchToProps = {
  setisIntro,
  setUser,
  setUrlserver,
};

export default connect(mapStateProps, mapDispatchToProps)(FBLoginButton);
