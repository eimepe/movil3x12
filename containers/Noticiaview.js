import React from 'react';
import {View, Text, StyleSheet, Image, Dimensions} from 'react-native';
import HTML from 'react-native-render-html';
import {ScrollView} from 'react-native-gesture-handler';
import {connect} from 'react-redux';
import {color} from 'react-native-reanimated';
import Constants from '../Utils/Constants';
class Noticiaview extends React.Component {
  render() {
    const noti = this.props.route.params.itemId;
    console.log(noti.photo);
    if (!this.props.user.user) {
      return <View></View>;
    } else {
      return (
        <ScrollView style={styles.container}>
          <Text style={styles.title}>{noti.title}</Text>
          <Image
            source={{uri: `${Constants.URLFILES}/images/news/` + noti.photo}}
            style={styles.image}
          />
          <Text>{noti.description}</Text>
          <HTML
            html={noti.content}
            imagesMaxWidth={Dimensions.get('window').width - 20}
          />
        </ScrollView>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 10,
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    fontWeight: 'bold',
    paddingTop: 10,
    paddingBottom: 10,
    color: '#f41d30',
  },
  image: {
    width: Dimensions.get('window').width - 20,
    marginBottom: 20,
    height: 200,
  },
});

const mapStateProps = (state) => {
  return {
    isIntro: state.isIntro,
    urlserver: state.urlserver,
    user: state.user,
    noticias: state.noticias,
  };
};

export default connect(mapStateProps, null)(Noticiaview);
