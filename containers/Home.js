
import * as React from 'react';
import {TouchableOpacity, Text} from 'react-native'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/FontAwesome';
import {connect} from 'react-redux'
import {setUser} from '../store/actions'
import Piramide from './drawer/Piramide'
import Faq from './Faq'
import Home from './drawer/Home'
import Noticiaview from './Noticiaview'
const Tab = createBottomTabNavigator();
import { createDrawerNavigator } from '@react-navigation/drawer';
import Contacto from './Contacto';
import Noticias from './Noticias';
import Lineal from './drawer/Lineal';
import Cerrarsesion from './drawer/Cerrarsesion';


const Drawer = createDrawerNavigator();

const SettingsStack = createStackNavigator();

function FAQs() {
  return (
    <SettingsStack.Navigator
    screenOptions={{
      headerShown: true
  }}
    >
      <SettingsStack.Screen name="FAQ" component={Faq} 
             options={{
              title: 'Preguntas frecuentes',
              headerStyle: {
              backgroundColor: '#d32f2f',
              },
              headerTintColor: '#fff',
              headerTitleAlign: 'center',
              
          }}/>
     
    </SettingsStack.Navigator>
  );
}

function Pirami() {
  return (
    <SettingsStack.Navigator
    screenOptions={{
      headerShown: true
  }}
    >
      <SettingsStack.Screen name="FAQ" component={Piramide} 
             options={{
              title: 'Mis Referidos',
              headerStyle: {
              backgroundColor: '#d32f2f',
              },
              headerTintColor: '#fff',
              headerTitleAlign: 'center',
              
          }}/>
     
    </SettingsStack.Navigator>
  );
}

function Line() {
  return (
    <SettingsStack.Navigator
    screenOptions={{
      headerShown: true
  }}
    >
      <SettingsStack.Screen name="Lineal" component={Lineal} 
             options={{
              title: 'Estructura lineal',
              headerStyle: {
              backgroundColor: '#d32f2f',
              },
              headerTintColor: '#fff',
              headerTitleAlign: 'center',
              
          }}/>
     
    </SettingsStack.Navigator>
  );
}


function MyACOUNT() {
  return (
    <SettingsStack.Navigator
    screenOptions={{
      headerShown: true
  }}
    >
      <SettingsStack.Screen name="Myaccount" component={Home} 
             options={{
              title: 'Mi cuenta',
              headerStyle: {
              backgroundColor: '#d32f2f',
              },
              headerTintColor: '#fff',
              headerTitleAlign: 'center',
              
          }} />
     
    </SettingsStack.Navigator>
  );
}

function Contact() {
  return (
    <SettingsStack.Navigator
    screenOptions={{
      headerShown: true
  }}
    >
      <SettingsStack.Screen name="Contacto" component={Contacto} 
             options={{
              title: 'Contactenos',
              headerStyle: {
              backgroundColor: '#d32f2f',
              },
              headerTintColor: '#fff',
              headerTitleAlign: 'center',
              
          }} />
     
    </SettingsStack.Navigator>
  );
}

function Noti() {
  return (
    <SettingsStack.Navigator
    screenOptions={{
      headerShown: true
  }}
    >
      <SettingsStack.Screen name="Noticias" component={Noticias} 
             options={{
              title: 'Noticias',
              headerStyle: {
              backgroundColor: '#d32f2f',
              },
              headerTintColor: '#fff',
              headerTitleAlign: 'center',
              
          }}
      />
      <SettingsStack.Screen name="Noticiaview" component={Noticiaview}  independent={true} 
                                            options={{
                                                title: 'Detalle',
                                                headerStyle: {
                                                backgroundColor: '#d32f2f',
                                                },
                                                headerTintColor: '#fff',
                                                headerTitleAlign: 'center',
                                                
                                            }} 
                                        />
    </SettingsStack.Navigator>
  );
}

class Homes extends React.Component{

    render(){
        return (
          
              <Tab.Navigator
            style={{border: 'green'}}
            screenOptions={{
              headerShown: true
          }}
            tabBarOptions={{
              style: {
                height: 80,
                backgroundColor: '#B9181A'
               },
               labelStyle: {
                fontSize: 15,
                margin: 0,
                color: 'white',
                padding: 0,
              },
             
              activeTintColor: 'white',
              inactiveTintColor: '#ccc',
              
            }}
            >
                

                
                <Tab.Screen name="Home" component={MyACOUNT}
                  options={{
                    tabBarLabel: 'Mi cuenta',
                    tabBarIcon: ({ color, size }) => (
                      <Icon name="user" color={color} size={size} />
                    ),
                  }}
    />
                <Tab.Screen name="FAQ" component={FAQs} options={{title: "FAQ"}} 
                  options={{
                    tabBarLabel: 'FAQS',
                    tabBarIcon: ({ color, size }) => (
                      <Icon name="question-circle" color={color} size={size} />
                    ),
                  }}
   />
                
                <Tab.Screen name="Noticias" component={Noti}
                  options={{
                    tabBarLabel: 'Noticias',
                    tabBarIcon: ({ color, size }) => (
                      <Icon name="id-card" color={color} size={size}
                       />
                    ),
                  }}
   />
                <Tab.Screen name="Contacto" component={Contact}
                      options={{
                        tabBarLabel: 'Contacto',
                        tabBarIcon: ({ color, size }) => (
                          <Icon name="phone" color={color} size={size}
                           />
                        ),
                      }} />


            </Tab.Navigator>
          
       
        );
    }
  
}


class RightDrawer extends React.Component {

  constructor(props){
    super(props);
    props.navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity
        
        onPress={() => props.navigation.dispatch(DrawerActions.toggleDrawer())}
      >
        <Text style={{marginLeft: 10}}> <Icon name="bars" size={20} color="#fff" /></Text>
      </TouchableOpacity>
        
      ),
    });
  }
  render(){
    return (
      <Drawer.Navigator initialRouteName="Homes" drawerPosition="left">
         <Drawer.Screen name="Homes" component={Homes} options={{title: "Mi Cuenta"}} />
         
          <Drawer.Screen name="miestructura" component={Pirami}
              options={{
                title: 'Mi Equipo',
                headerStyle: {
                backgroundColor: '#d32f2f',
                },
                headerTintColor: '#fff',
                headerTitleAlign: 'center',
                
            }} 
        />

<Drawer.Screen name="lineal" component={Line}
              options={{
                title: 'Estructura Lineal',
                headerStyle: {
                backgroundColor: '#d32f2f',
                },
                headerTintColor: '#fff',
                headerTitleAlign: 'center',
                
            }} 
        />
  <Drawer.Screen name="Cerrarseion" component={Cerrarsesion}
              options={{
                title: 'Cerrar sesion',
                headerStyle: {
                backgroundColor: '#d32f2f',
                },
                headerTintColor: '#fff',
                headerTitleAlign: 'center',
                
            }} 
        />
      </Drawer.Navigator>
    )
  }
  
}

const mapStateProps = (state)=>{
    return{
        user: state.user,
        urlserver: state.urlserver
    
    }
  }
  
  
  
  const mapDispatchToProps = {
    setUser,
  
  }
export default connect(mapStateProps, mapDispatchToProps) (RightDrawer);
