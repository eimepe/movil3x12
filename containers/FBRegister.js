import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {setUser, setisIntro, setUrlserver} from '../store/actions'
import {
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager,
} from 'react-native-fbsdk';
import { connect } from 'react-redux';


 class FBRegister extends Component {
     constructor(props){
         super(props);
         this.register2 = this.register2.bind(this);
         this.state = {userInfo: {}};
     }
 

  logoutWithFacebook = () => {
    LoginManager.logOut();
    this.setState({userInfo: {}});
  };

  getInfoFromToken = token => {
    const PROFILE_REQUEST_PARAMS = {
      fields: {
        string: 'id,name,first_name,last_name',
      },
    };
    const profileRequest = new GraphRequest(
      '/me',
      {token, parameters: PROFILE_REQUEST_PARAMS},
      (error, user) => {
        if (error) {
          console.log('login info has error: ' + error);
        } else {
          this.setState({userInfo: user});
          console.log('result:', user);
          this.register2(user);
        }
      },
    );
    new GraphRequestManager().addRequest(profileRequest).start();
  };

  register2(user){
        console.log(Navigation);
  }
  loginWithFacebook = () => {
    // Attempt a login using the Facebook login dialog asking for default permissions.
    LoginManager.logInWithPermissions(['public_profile']).then(
      login => {
        if (login.isCancelled) {
          console.log('Login cancelled');
        } else {
          AccessToken.getCurrentAccessToken().then(data => {
            const accessToken = data.accessToken.toString();
            this.getInfoFromToken(accessToken);
          });
        }
      },
      error => {
        console.log('Login fail with error: ' + error);
      },
    );
  };

 

  render() {
   
    return (
      <View style={{flex: 1, margin: 50}}>
        <TouchableOpacity
          onPress={onPressButton}
          style={styles.botonface}>
          <Text style={styles.textboton}>{buttonText}</Text>
        </TouchableOpacity>
        {this.state.userInfo.name && (
          <Text style={styles.textboton}>
           Logeado como {this.state.userInfo.name}
          </Text>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 10
  },
  textboton:{
    color: "white"
  },
  botoniniciar: {
    marginBottom: 20,
    width: 300,
    alignItems: "center",
    backgroundColor: "#EED626",
    padding: 10,
    borderRadius: 40
  },
  botonface: {
    marginBottom: 20,
    width: 300,
    alignItems: "center",
    backgroundColor: "#365BF4",
    padding: 10,
    borderRadius: 40
  },
  countContainer: {
    alignItems: "center",
    padding: 10
  }
});

const mapStateProps = (state)=>{
  return{
      isIntro: state.isIntro,
      urlserver: state.urlserver,
      user: state.user
  
  }
}



const mapDispatchToProps = {
  setisIntro,
  setUser,
  setUrlserver
  

}



export default connect(mapStateProps, mapDispatchToProps)(FBRegister);
  