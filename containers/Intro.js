import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import {connect} from 'react-redux'
import {setIsintro } from '../store/actions'
//import Icon from 'react-native-vector-icons/Ionicons';






const slides = [
  {
    key: 'somethun',
    title: 'Primera',
    text: 'Description.\nSay something cool',
    image: require('../assets/logo.png'),

    backgroundColor: '#D62227',
  },
  {
    key: 'somethun-dos',
    title: 'Segunda',
    text: 'Other cool stuff',
    image: require('../assets/logo.png'),

    backgroundColor: '#D62227',
  },
  {
    key: 'somethun1',
    title: 'Tercera',
    text: 'I\'m already out of descriptions\n\nLorem ipsum bla bla bla',
    image: require('../assets/logo.png'),

    backgroundColor: '#D62227',
  }
];

 class Intro extends React.Component {
  static navigationOptions = {
    header: null
  }
  constructor(props){
    super(props);
    this.state = {
      showRealApp: false
    }
  }

  componentDidMount(){
  
  }

  _renderItem = ({ item }) => {
    return (
      <View style={styles.slide}>
        <Text style={styles.title}>{item.title}</Text>
        <Image source={item.image} style={{width: 200, height: 200}}  />
        <Text style={styles.text}>{item.text}</Text>
      </View>
    );
  }

  

 
  
  _onDone = () => {
    // User finished the introduction. Show real app through
    // navigation or simply by controlling state
    this.props.setIsintro(true);
    this.props.navigation.navigate('Login')
  }
  render() {
     
  
          return <AppIntroSlider 
              renderItem={this._renderItem}
              data={slides}
              onDone={this._onDone}
              //renderDoneButton={this._renderDoneButton}
              //renderNextButton={this._renderNextButton}
            />;;
        
      
    
  }
}



const styles = StyleSheet.create({
  slide: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#D62227',
    color: '#EBCF25'
  },
  image: {
    width: 320,
    height: 320,
    marginVertical: 32,
  },
  text: {
    color: '#EBCF25',
    textAlign: 'center',
  },
  title: {
    fontSize: 22,
    color: '#EBCF25',
    textAlign: 'center',
  },
  buttonCircle: {
    width: 44,
    height: 44,
    backgroundColor: 'rgba(0, 0, 0, .2)',
    borderRadius: 22,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
const mapStateProps = (state)=>{
  return{
      isIntro: state.isIntro,
  
  }
}



const mapDispatchToProps = {
  setIsintro,

}
export default connect(mapStateProps, mapDispatchToProps) (Intro);
