import React from 'react'
import {View, TextInput, Image, Text, TouchableOpacity, StyleSheet} from 'react-native';
import { connect } from 'react-redux';
import Constants from '../Utils/Constants';

class Resetpass extends React.Component{
    constructor(){
        super();
        this.state={
            email: "",
            status: 0,
            codigo:"",
            codetrue: "",
            response: ""
        }
    }

    reset(){
  
            console.log(Constants.URL);
            fetch(Constants.URL+'/resetpass', {
             method: 'POST',
             headers: {
               'Accept': 'application/json, text/plain, */*',
               'Content-Type': 'application/json',
              
             },
             body: JSON.stringify({
              "email":this.state.email,
             }),
        
            }).then(res=>res.json())
             .then(res => {
               console.log(res.response);
               if(res.response=="1"){
                    this.setState({status: 1});
               }else{
                   
               }
            
             }).catch((error) => {
               console.log(error);
               
             });
           
    }

    resetCode(){
  
 
        fetch(Constants.URL+'/resetpass?code='+this.state.codigo, {
         method: 'GET',
         headers: {
           'Accept': 'application/json, text/plain, */*',
           'Content-Type': 'application/json',
          
         },
       /*  body: JSON.stringify({
          "email":this.state.email,
         }),
    */
        }).then(res=>res.json())
         .then(res => {
           console.log(res.response);
           if(res.response=="1"){
                this.setState({codetrue: 1});
                this.props.navigation.navigate('Newpass',{
                    id: res.id,
                    
                  })
           }else{
                this.setState({response: "El codigo no es valido o ya caduco intente de nuevo."})
           }
        
         }).catch((error) => {
           console.log(error);
           
         });
       
}

    onChangeText(text){
        this.setState({email: text});
    }

    onChangeTextC(text){
        this.setState({codigo: text});
    }
    render(){
        if(this.state.status==0){

        
        return(
            <View style={{flex: 1,padding: 30, backgroundColor: '#D62227', alignItems: "center"}}>
            <Image source={require('../assets/logo.png')} style={{ width: 300, height: 300, }} />
            <TextInput
                style={{ width: 300, height: 40, borderColor: 'yellow', borderWidth: 1, borderRadius: 40, marginBottom: 30, paddingLeft: 40, backgroundColor: 'white' }}
                onChangeText={text => this.onChangeText(text)}
                value={this.state.email}
                placeholder="Correo"
            />
          
    
           
            <TouchableOpacity
              style={styles.botoniniciar}
              onPress={this.reset.bind(this)}
            >
              <Text style={styles.textboton}>Recuperar</Text>
            </TouchableOpacity>
    
    
          
            </View>
        )
        }else{
            return(
              
            <View style={{flex: 1,padding: 30, backgroundColor: '#D62227', alignItems: "center"}}>
            <Image source={require('../assets/logo.png')} style={{ width: 300, height: 300, }} />
            <Text style={{fontSize: 20, color: "white", marginBottom: 10}}>Escriba el codigo que recibio en su correo</Text>
            <TextInput
                style={{ width: 300, height: 40, borderColor: 'yellow', borderWidth: 1, borderRadius: 40, marginBottom: 30, paddingLeft: 40, backgroundColor: 'white' }}
                onChangeText={text => this.onChangeTextC(text)}
                value={this.state.codigo}
                placeholder="Codigo Recibido"
            />
            
             <Text style={{color: "black", fontSize: 20}}>{this.state.response}</Text>
           
            <TouchableOpacity
              style={styles.botoniniciar}
              onPress={this.resetCode.bind(this)}
            >
              <Text style={styles.textboton}>Enviar</Text>
            </TouchableOpacity>
    
    
          
            </View> 
                
            )
        }
    }
    
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      paddingHorizontal: 10
    },
    textboton:{
      color: "#000"
    },
    botoniniciar: {
      marginBottom: 20,
      width: 300,
      alignItems: "center",
      backgroundColor: "#EED626",
      padding: 10,
      borderRadius: 40
    },
    botonface: {
      marginBottom: 20,
      width: 300,
      alignItems: "center",
      backgroundColor: "#365BF4",
      padding: 10,
      borderRadius: 40
    },
    countContainer: {
      alignItems: "center",
      padding: 10
    }
  });
  const mapStateProps = (state)=>{
    return{
        isIntro: state.isIntro,
        urlserver: state.urlserver,
        user: state.user,
        noticias: state.noticias
    
    }
  }

export default connect(mapStateProps, null) (Resetpass);