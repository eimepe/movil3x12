import * as React from 'react';
//import { TextInput } from 'react-native-paper';
import {View, TextInput, Image, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {connect} from 'react-redux'
import {setUser, setisIntro, setUrlserver} from '../store/actions'
import FBLoginButton from './FBLoginButton'
import Modal from 'react-native-modal';
import { Appbar, Button } from 'react-native-paper';
import Constants from '../Utils/Constants'
class Login extends React.Component {
 
  constructor(props){
    super(props);
    this.state = {
      email: "",
      password: "",
      visibleok: false,
      contentmodalok: ""
    }
  }

  componentDidMount(){
    
  }

  login(){ 

    fetch(Constants.URL+'/authentication', {
     method: 'POST',
     headers: {
       'Accept': 'application/json, text/plain, */*',
       'Content-Type': 'application/json',
      
     },
     body: JSON.stringify({
      "email":this.state.email,
      "password": this.state.password,
      "strategy": "local" 
     }),

    }).then(res=>res.json())
     .then(res => {
       console.log(res);
       
     if(res.accessToken){
       this.props.setUser(res)
       console.warn(res);
       this.props.navigation.navigate('Home');
       
     }else{
       console.warn("error de autenticacion");
       this.setState({visibleok: true, contentmodalok: "Error de usuario o contraseña"});
       
     }
     }).catch((error) => {
       console.log(error);
       
     });
   }

   onChangeText(text){
     console.log(text);
     this.setState({email: text})
   }

   onChangeTextP(text){
    console.log(text);
    this.setState({password: text})
  }

render(){
 
  return (
      <View style={{flex: 1,padding: 30, backgroundColor: '#D62227', alignItems: "center"}}>
        <Image source={require('../assets/logo.png')} style={{ width: 300, height: 300, }} />
        <TextInput
            style={{ width: 300, height: 40, borderColor: 'yellow', borderWidth: 1, borderRadius: 40, marginBottom: 30, paddingLeft: 40, backgroundColor: 'white' }}
            onChangeText={text => this.onChangeText(text)}
            value={this.state.email}
            placeholder="Correo"
        />
        <TextInput
            style={{ width: 300, height: 40, borderColor: 'yellow', borderWidth: 1, borderRadius: 40, marginBottom: 30, paddingLeft: 40, backgroundColor: 'white' }}
            onChangeText={text => this.onChangeTextP(text)}
            value={this.state.password}
            placeholder="Contraseña"
        />

       
        <TouchableOpacity
          style={styles.botoniniciar}
          onPress={this.login.bind(this)}
        >
          <Text style={styles.textboton}>Entrar</Text>
        </TouchableOpacity>
        

        <FBLoginButton/>

        <Text onPress={()=>this.props.navigation.navigate('Resetpass')} style={{ marginBottom: 20}}>Olvidaste tu contraseña?</Text>
        <TouchableOpacity
          style={styles.botoniniciar}
          onPress={()=>this.props.navigation.navigate("Register")}
        >
          <Text style={styles.textboton}>Registrarse</Text>
        </TouchableOpacity>
        <Modal isVisible={this.state.visibleok}>
                  <View style={styles.modal}>
                    <Text>{this.state.contentmodalok}</Text>
                  
                  <Button onPress={()=>this.setState({visibleok: false})}>
                      OK
                  </Button>
                  </View>
                </Modal>
        </View>
    
  );
}
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 10
  },
  modal: {
    backgroundColor: 'white',
    borderRadius: 10,
    minHeight: 80,
    padding: 10
  },
  textboton:{
    color: "#21242a"
  },
  botoniniciar: {
    marginBottom: 20,
    width: 300,
    alignItems: "center",
    backgroundColor: "#EED626",
    padding: 10,
    borderRadius: 40
  },
  botonface: {
    marginBottom: 20,
    width: 300,
    alignItems: "center",
    backgroundColor: "#365BF4",
    padding: 10,
    borderRadius: 40
  },
  countContainer: {
    alignItems: "center",
    padding: 10
  }
});

const mapStateProps = (state)=>{
  return{
      isIntro: state.isIntro,
      urlserver: state.urlserver,
      user: state.user
  
  }
}



const mapDispatchToProps = {
  setisIntro,
  setUser,
  setUrlserver
  

}



export default connect(mapStateProps, mapDispatchToProps)(Login);
  