import * as React from 'react';
import {List} from 'react-native-paper';
import {Text, ScrollView} from 'react-native';
import {connect} from 'react-redux';
import {setUser, setisIntro, setUrlserver} from '../store/actions';
import Constants from '../Utils/Constants';

class Faq extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      faqs: [],
    };
  }
  getFaq() {
    fetch(Constants.URL + '/faqs?$limit=1000', {
      method: 'GET',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);

        if (res.total > 0) {
          this.setState({faqs: res.data});
          console.log(res.data);
        } else {
          console.warn('error de autenticacion');
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
  componentDidMount() {
    this.getFaq();
  }
  render() {
    if (!this.props.user.user) {
      return <View></View>;
    } else {
      return (
        <ScrollView>
          <List.Section>
            {this.state.faqs.map((faq) => {
              return (
                <List.Accordion
                  title={faq.question}
                  titleNumberOfLines={4}
                  style={{paddingTop: 0}}
                  left={(props) => <List.Icon {...props} icon="help-circle" />}>
                  <List.Item
                    titleStyle={{height: 0}}
                    title={null}
                    descriptionNumberOfLines={50}
                    description={faq.answer}
                  />
                </List.Accordion>
              );
            })}
          </List.Section>
        </ScrollView>
      );
    }
  }
}
const mapStateProps = (state) => {
  return {
    isIntro: state.isIntro,
    urlserver: state.urlserver,
    user: state.user,
  };
};

const mapDispatchToProps = {
  setisIntro,
  setUser,
  setUrlserver,
};
export default connect(mapStateProps, mapDispatchToProps)(Faq);
