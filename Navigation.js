import React from 'react';
import {Button, View, Text} from 'react-native';
import {connect} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {setisIntro} from './store/actions';

//Vistas imports
import HomeScreen from './containers/Home';
import Intro from './containers/Intro';
import Login from './containers/Login';
import Resetpass from './containers/Resetpass';
import Newpass from './containers/Newpass';
import Noticias from './containers/Noticias';
import Noticiaview from './containers/Noticiaview';
import Register from './containers/Register';
import Register2 from './containers/Register2';

import Pagoinicial from './containers/Pagoinicial';
import Imageperfil from './componens/editperfil/Imageperfil';

const Stack = createStackNavigator();

class Navigations extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}>
          {!this.props.isIntro ? (
            <Stack.Screen
              name="Intro"
              component={Intro}
              independent={true}
              options={{
                headerStyle: {
                  backgroundColor: '#d32f2f',
                },
                headerTintColor: '#fff',
                headerTitleAlign: 'center',
              }}
            />
          ) : null}
          {!this.props.user.user ? (
            <Stack.Screen
              name="Login"
              component={Login}
              options={{
                title: 'Iniciar Sesion',
                headerStyle: {
                  backgroundColor: '#d32f2f',
                },
                headerTintColor: '#fff',
                headerTitleAlign: 'center',
              }}
            />
          ) : (
            <Stack.Screen
              name="Home"
              component={HomeScreen}
              options={{
                title: '3x13',
                headerStyle: {
                  backgroundColor: '#d32f2f',
                },
                headerTintColor: '#fff',
                headerTitleAlign: 'center',
              }}
            />
          )}
          <Stack.Screen
            name="Resetpass"
            component={Resetpass}
            independent={false}
            options={{
              title: 'Olvidaste tu Contraseña?',
              headerStyle: {
                backgroundColor: '#d32f2f',
              },
              headerTintColor: '#fff',
              headerTitleAlign: 'center',
            }}
          />
          <Stack.Screen
            name="Newpass"
            component={Newpass}
            independent={false}
            options={{
              title: 'Olvidaste tu Contraseña?',
              headerStyle: {
                backgroundColor: '#d32f2f',
              },
              headerTintColor: '#fff',
              headerTitleAlign: 'center',
            }}
          />

          <Stack.Screen
            name="Register"
            component={Register}
            independent={false}
            options={{
              title: 'Registro de usuario?',
              headerStyle: {
                backgroundColor: '#d32f2f',
              },
              headerTintColor: '#fff',
              headerTitleAlign: 'center',
            }}
          />
          <Stack.Screen
            name="Register2"
            component={Register2}
            independent={false}
            options={{
              title: 'Terminos y condiciones?',
              headerStyle: {
                backgroundColor: '#d32f2f',
              },
              headerTintColor: '#fff',
              headerTitleAlign: 'center',
            }}
          />

          <Stack.Screen
            name="pagoinicial"
            component={Pagoinicial}
            independent={false}
            options={{
              title: 'Pago inicial',
              headerStyle: {
                backgroundColor: '#d32f2f',
              },
              headerTintColor: '#fff',
              headerTitleAlign: 'center',
            }}
          />

          <Stack.Screen
            name="camara"
            component={Imageperfil}
            independent={false}
            options={{
              title: 'Pago inicial',
              headerStyle: {
                backgroundColor: '#d32f2f',
              },
              headerTintColor: '#fff',
              headerTitleAlign: 'center',
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const mapStateProps = (state) => {
  return {
    isIntro: state.isIntro,
    urlserver: state.urlserver,
    user: state.user,
  };
};

const mapDispatchToProps = {
  setisIntro,
};

export default connect(mapStateProps, mapDispatchToProps)(Navigations);
